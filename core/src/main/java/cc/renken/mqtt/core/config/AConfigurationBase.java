/**
 * 
 */
package cc.renken.mqtt.core.config;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Properties;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;

import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.internal.websocket.Base64;

import cc.renken.mqtt.core.api.Topic;

/**
 * @author renkenh
 *
 */
public abstract class AConfigurationBase extends AConfigValueContainer
{
	
	private static final MessageDigest DIGEST;
	static
	{
		try
		{
			DIGEST = MessageDigest.getInstance("SHA-256");
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new RuntimeException("Hashing with SHA-256 not supported.");
		}
	}
	

	private String configHash = null;

	public AConfigurationBase()
	{
		super();
	}

	public AConfigurationBase(AOptionsBase<? extends AConfigurationBase> options)
	{
		super(options);
	}
	
	protected AConfigurationBase(AConfigurationBase config)
	{
		super(config);
	}
	
	
	protected abstract AConfigurationBase defaultConfiguration();

	
	public final String getID()
	{
		if (this.configHash == null)
		{
			StringBuilder sb = new StringBuilder();
			this.createStringRepresentation(sb);
			byte[] hash = DIGEST.digest(sb.toString().getBytes(StandardCharsets.UTF_8));
			this.configHash = Base64.encodeBytes(hash);
		}
		return this.configHash;
	}

	protected void createStringRepresentation(StringBuilder sb)
	{
		sb.append(this.getAutomaticReconnect());
		if (this.getAutomaticReconnect())
			sb.append(this.getMaxReconnectDelay().compareTo(Duration.ofMillis(0)) > 0);
		sb.append(this.getTopicBase());
		for (String uri : this.getServerURIs())
			sb.append(uri);
		sb.append(this.getMqttVersion());
		sb.append(this.getKeepAliveInterval().compareTo(Duration.ofMillis(0)) > 0);
		sb.append(this.getCleanSession());
		sb.append(this.getConnectionTimeout().compareTo(Duration.ofMillis(0)) > 0);
		sb.append(this.getPersistence().getClass().getName());
		sb.append(this.getHttpsHostnameVerificationEnabled());
		Properties headers = this.getCustomWebSocketHeaders();
		if (headers != null)
			headers.forEach((k,v) -> { sb.append(k); sb.append(v); });
		Properties ssl = this.getSSLClientProperties();
		if (ssl != null)
			ssl.forEach((k,v) -> { sb.append(k); sb.append(v); });
	}

	/**
	 * @return the keepAliveInterval
	 */
	@Override
	public Duration getKeepAliveInterval()
	{
		Duration interval = super.getKeepAliveInterval();
		if (interval == null)
			return this.defaultConfiguration().getKeepAliveInterval();
		return interval;
	}

	/**
	 * @return the maxInflight
	 */
	public int getMaxInflight()
	{
		if (this.maxInflight() == null)
			return this.defaultConfiguration().getMaxInflight();
		return this.maxInflight().intValue();
	}

	/**
	 * @return the cleanSession
	 */
	public boolean getCleanSession()
	{
		if (this.cleanSession() == null)
			return this.defaultConfiguration().getCleanSession();
		return this.cleanSession().booleanValue();
	}

	/**
	 * @return the connectionTimeout
	 */
	public Duration getConnectionTimeout()
	{
		Duration timeout = super.getConnectionTimeout();
		if (timeout == null)
			return this.defaultConfiguration().getConnectionTimeout();
		return timeout;
	}
	
	/**
	 * @return the mqttVersion
	 */
	public MQTTVersion getMqttVersion()
	{
		MQTTVersion v = super.getMqttVersion();
		if (v == null)
			return this.defaultConfiguration().getMqttVersion();
		return v;
	}
	
	/**
	 * @return the automaticReconnect
	 */
	public boolean getAutomaticReconnect()
	{
		Boolean reconnect = this.automaticReconnect();
		if (reconnect == null)
			return this.defaultConfiguration().getAutomaticReconnect();
		return reconnect.booleanValue();
	}
	
	/**
	 * @return the maxReconnectDelay
	 */
	public Duration getMaxReconnectDelay()
	{
		Duration delay = super.getMaxReconnectDelay();
		if (delay == null)
			return this.defaultConfiguration().getMaxReconnectDelay();
		return delay;
	}
	
	/**
	 * @return the customWebSocketHeaders
	 */
	public Properties getCustomWebSocketHeaders()
	{
		Properties p = super.getCustomWebSocketHeaders();
		if (p == null)
			p = this.defaultConfiguration().getCustomWebSocketHeaders();
		if (p == null)
			return null;
		return new Properties(p);
	}
	
	/**
	 * @return the qos
	 */
	public int getQoS()
	{
		Integer qos = this.qos();
		if (qos == null)
			return this.defaultConfiguration().getQoS();
		return qos.intValue();
	}
	
	/**
	 * @return the retain
	 */
	public boolean getRetain()
	{
		Boolean retain = this.retain();
		if (retain == null)
			return this.defaultConfiguration().getRetain();
		return retain.booleanValue();
	}

	/**
	 * @return the persitence
	 */
	public MqttClientPersistence getPersistence()
	{
		MqttClientPersistence p = super.getPersistence();
		if (p == null)
			return this.defaultConfiguration().getPersistence();
		return p;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		String username = super.getUserName();
		if (username == null)
			return this.defaultConfiguration().getUserName();
		return username;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword()
	{
		String password = super.getPassword();
		if (password == null)
			return this.defaultConfiguration().getPassword();
		return password;
	}
	
	/**
	 * @return the socketFactory
	 */
	public SocketFactory getSocketFactory()
	{
		SocketFactory factory = super.getSocketFactory();
		if (factory == null)
			return this.defaultConfiguration().getSocketFactory();
		return factory;
	}

	/**
	 * @return the sslClientProps
	 */
	public Properties getSSLClientProperties()
	{
		Properties p = super.getSSLClientProperties();
		if (p == null)
			p = this.defaultConfiguration().getSSLClientProperties();
		if (p == null)
			return null;
		return new Properties(p);
	}

	/**
	 * @return the httpsHostnameVerificationEnabled
	 */
	public boolean getHttpsHostnameVerificationEnabled()
	{
		if (super.httpsHostnameVerification() == null)
			return this.defaultConfiguration().getHttpsHostnameVerificationEnabled();
		return super.httpsHostnameVerification().booleanValue();
	}

	/**
	 * @return the sslHostnameVerifier
	 */
	public HostnameVerifier getSSLHostnameVerifier()
	{
		HostnameVerifier verifier = super.getSSLHostnameVerifier();
		if (verifier == null)
			return this.defaultConfiguration().getSSLHostnameVerifier();
		return verifier;
	}

	/**
	 * @return the serverURIs
	 */
	public String[] getServerURIs()
	{
		String[] uris = super.getServerURIs();
		if (uris == null)
			return this.defaultConfiguration().getServerURIs();
		return uris;
	}

	public IMQTTClientIDGenerator<AConfigurationBase> getClientIDGenerator()
	{
		IMQTTClientIDGenerator<AConfigurationBase> generator = super.getClientIDGenerator();
		if (generator == null)
			return this.defaultConfiguration().getClientIDGenerator();
		return generator;
	}

	/**
	 * @return the topicBase
	 */
	@Override
	public Topic getTopicBase()
	{
		if (super.getTopicBase() == null)
			return this.defaultConfiguration().getTopicBase();
		return this.defaultConfiguration().getTopicBase().resolve(super.getTopicBase());
	}

}

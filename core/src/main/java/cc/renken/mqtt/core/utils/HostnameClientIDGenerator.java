/**
 * 
 */
package cc.renken.mqtt.core.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.mqtt.core.config.AConfigurationBase;

/**
 * @author renkenh
 *
 */
public class HostnameClientIDGenerator<C extends AConfigurationBase> extends PrefixClientIDGenerator<AConfigurationBase>
{
	private static final Logger logger = LoggerFactory.getLogger(HostnameClientIDGenerator.class);
	
	public HostnameClientIDGenerator()
	{
		super(getHostname());
	}

	public static final String getHostname()
	{
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			logger.debug("Could not extract hostname.", e);
		}
		return "unknown";
	}
}

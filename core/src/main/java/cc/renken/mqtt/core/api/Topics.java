/**
 * 
 */
package cc.renken.mqtt.core.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author renkenh
 *
 */
public final class Topics
{
	
	public static final String SEPARATOR = "/";


	public static final Topic get(String first, String... more)
	{
		ArrayList<String> list = new ArrayList<>(more.length+1);
		list.add(first);
		list.addAll(Arrays.asList(more));
		return Topics.get(list);
	}
	
	public static final Topic get(List<String> parts)
	{
        return new Topic(parts.stream().filter((p) -> p != null).collect(Collectors.toList()));
	}

	private Topics()
	{
		//no instance
	}

}

package cc.renken.mqtt.core.config;

public enum Persistence
{
	IN_MEMORY,
	FILE
}


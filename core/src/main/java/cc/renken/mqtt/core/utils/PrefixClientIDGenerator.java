/**
 * 
 */
package cc.renken.mqtt.core.utils;

import cc.renken.mqtt.core.config.AConfigurationBase;
import cc.renken.mqtt.core.config.IMQTTClientIDGenerator;

/**
 * @author renkenh
 *
 */
public class PrefixClientIDGenerator<C extends AConfigurationBase> implements IMQTTClientIDGenerator<C>
{
	
	private final String prefix;
	
	public PrefixClientIDGenerator(String prefix)
	{
		this.prefix = prefix;
	}


	@Override
	public String generate(C config)
	{
		return this.prefix + System.nanoTime();
	}

}

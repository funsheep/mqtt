/**
 * 
 */
package cc.renken.mqtt.core.config;

/**
 * @author renkenh
 *
 */
public class ServiceConfiguration extends AConfigurationBase
{
	
	private static final DefaultServiceConfiguration DEFAULT_CONFIG = new DefaultServiceConfiguration();
	
	
	public ServiceConfiguration(ServiceOptions options)
	{
		super(options);
	}
	
	public ServiceConfiguration(ServiceConfiguration config)
	{
		super(config);
	}


	@Override
	protected AConfigurationBase defaultConfiguration()
	{
		return DEFAULT_CONFIG;
	}


}

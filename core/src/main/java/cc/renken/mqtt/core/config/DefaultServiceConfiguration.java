/**
 * 
 */
package cc.renken.mqtt.core.config;

import java.time.Duration;
import java.util.Properties;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import cc.renken.mqtt.core.api.Topic;
import cc.renken.mqtt.core.api.Topics;

/**
 * @author renkenh
 *
 */
class DefaultServiceConfiguration extends AConfigurationBase
{
	

	private static final MqttConnectOptions DEFAULT_OPTIONS = new MqttConnectOptions();
	
	
	protected DefaultServiceConfiguration()
	{
		super();
	}
	
	/**
	 * @return the topicBase
	 */
	@Override
	public Topic getTopicBase()
	{
		return Topics.get("");
	}

	/**
	 * @return the keepAliveInterval
	 */
	@Override
	public Duration getKeepAliveInterval()
	{
		return Duration.ofSeconds(MqttConnectOptions.KEEP_ALIVE_INTERVAL_DEFAULT);
	}
	
	/**
	 * @return the maxInflight
	 */
	@Override
	public int getMaxInflight()
	{
		return MqttConnectOptions.MAX_INFLIGHT_DEFAULT;
	}

	/**
	 * @return the userName
	 */
	@Override
	public String getUserName()
	{
		return DEFAULT_OPTIONS.getUserName();
	}

	/**
	 * @return the password
	 */
	@Override
	public String getPassword()
	{
		if (DEFAULT_OPTIONS.getPassword() != null)
			return new String(DEFAULT_OPTIONS.getPassword());
		return null;
	}

	/**
	 * @return the socketFactory
	 */
	@Override
	public SocketFactory getSocketFactory()
	{
		return DEFAULT_OPTIONS.getSocketFactory();
	}

	/**
	 * @return the sslClientProps
	 */
	@Override
	public Properties getSSLClientProperties()
	{
		return DEFAULT_OPTIONS.getSSLProperties();
	}

	/**
	 * @return the httpsHostnameVerificationEnabled
	 */
	@Override
	public boolean getHttpsHostnameVerificationEnabled()
	{
		return DEFAULT_OPTIONS.isHttpsHostnameVerificationEnabled();
	}

	/**
	 * @return the sslHostnameVerifier
	 */
	@Override
	public HostnameVerifier getSSLHostnameVerifier()
	{
		return DEFAULT_OPTIONS.getSSLHostnameVerifier();
	}
	
	/**
	 * @return the cleanSession
	 */
	@Override
	public boolean getCleanSession()
	{
		return MqttConnectOptions.CLEAN_SESSION_DEFAULT;
	}

	/**
	 * @return the connectionTimeout
	 */
	@Override
	public Duration getConnectionTimeout()
	{
		return Duration.ofSeconds(MqttConnectOptions.CONNECTION_TIMEOUT_DEFAULT);
	}

	/**
	 * @return the serverURIs
	 */
	@Override
	public String[] getServerURIs()
	{
		return new String[0];
	}

	/**
	 * @return the mqttVersion
	 */
	@Override
	public MQTTVersion getMqttVersion()
	{
		return MQTTVersion.DEFAULT;
	}

	/**
	 * @return the automaticReconnect
	 */
	@Override
	public boolean getAutomaticReconnect()
	{
		return DEFAULT_OPTIONS.isAutomaticReconnect();
	}

	/**
	 * @return the maxReconnectDelay
	 */
	@Override
	public Duration getMaxReconnectDelay()
	{
		return Duration.ofMillis(DEFAULT_OPTIONS.getMaxReconnectDelay());
	}

	/**
	 * @return the customWebSocketHeaders
	 */
	@Override
	public Properties getCustomWebSocketHeaders()
	{
		return DEFAULT_OPTIONS.getCustomWebSocketHeaders();
	}

	/**
	 * @return the qos
	 */
	@Override
	public int getQoS()
	{
		return 0;
	}

	/**
	 * @return the retain
	 */
	@Override
	public boolean getRetain()
	{
		return false;
	}

	/**
	 * @return the persitence
	 */
	@Override
	public MqttClientPersistence getPersistence()
	{
		return new MemoryPersistence();
	}
	
	/**
	 * @return the clientIDGenerator
	 */
	@Override
	public IMQTTClientIDGenerator<AConfigurationBase> getClientIDGenerator()
	{
		return (tc) -> MqttAsyncClient.generateClientId();
	}


	@Override
	protected AConfigurationBase defaultConfiguration()
	{
		return null;
	}

}

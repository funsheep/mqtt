/**
 * 
 */
package cc.renken.mqtt.core.impl;

import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.mqtt.core.api.IMQTTClient;
import cc.renken.mqtt.core.api.IMQTTListener;
import cc.renken.mqtt.core.api.Topic;
import cc.renken.mqtt.core.config.ClientConfiguration;

/**
 * @author renkenh
 *
 */
public class MQTTClient implements IMQTTClient
{
	
	private static class TopicListener
	{
		private final Topic topic;
		private final IMQTTListener listener;
		
		public TopicListener(Topic topic, IMQTTListener listener)
		{
			this.topic = topic;
			this.listener = listener;
		}
	}
	
	private static class MQTTListenerAdapter implements IMqttMessageListener
	{
		
		private final Deque<TopicListener> listeners = new ConcurrentLinkedDeque<>();

	
		@Override
		public void messageArrived(String mqttTopic, MqttMessage message) throws Exception
		{
			for (TopicListener listener : this.listeners)
			{
				MQTTRecvMessage recv = new MQTTRecvMessage(mqttTopic, listener.topic, message);
				listener.listener.handleMessage(recv);
			}
			logger.debug("{}({},{}) {}", mqttTopic, message.getQos(), message.isRetained(), new String(message.getPayload(), StandardCharsets.UTF_8));
		}

	}

	private static final Logger logger = LoggerFactory.getLogger(MQTTClient.class);
	
	private final ClientConfiguration config;
	private final IMqttClient client;
	private final MqttConnectOptions cachedOptions;

	private final Map<String, MQTTListenerAdapter> subscriptions = new HashMap<>();
	private final ReentrantLock subscriptionsLock = new ReentrantLock();


	/**
	 * 
	 */
	public MQTTClient(ClientConfiguration config, IMqttClient client)
	{
		this.config = config;
		this.cachedOptions = MQTTClientFactory.createOptions(config);
		this.client = client;
	}


	@Override
	public ClientConfiguration getConfig()
	{
		return this.config;
	}
	
	@Override
	public void connect() throws MqttException
	{
		if (this.isConnected())
			return;
		this.client.connect(this.cachedOptions);
	}
	
	@Override
	public boolean isConnected()
	{
		return this.client.isConnected();
	}
	
	@Override
	public void subscribe(Topic topic, IMQTTListener listener) throws MqttException
	{
		topic = this.config.getTopicBase().resolve(topic);
		this.subscriptionsLock.lock();
		try
		{
			MQTTListenerAdapter adapter = this.subscriptions.get(topic.toMQTT());
			if (adapter == null)
			{
				adapter = new MQTTListenerAdapter();
				this.client.subscribe(topic.toMQTT(), this.getConfig().getQoS(), adapter);	//TODO monitor qos - if qos too low - upgrade subscription
				this.subscriptions.put(topic.toMQTT(), adapter);
			}
			adapter.listeners.add(new TopicListener(topic, listener));
		}
		finally
		{
			this.subscriptionsLock.unlock();
		}
	}

	@Override
	public void unsubscribe(Topic topic, IMQTTListener listener)
	{
		topic = this.config.getTopicBase().resolve(topic);
		this.subscriptionsLock.lock();
		try
		{
			MQTTListenerAdapter adapter = this.subscriptions.get(topic.toMQTT());
			if (adapter == null)
				return;	//not subscribed
			final Topic tmp = topic;
			adapter.listeners.removeIf((l) -> l.topic.equals(tmp) && l.listener.equals(listener));
			if (adapter.listeners.isEmpty())
				this.client.unsubscribe(topic.toMQTT());
		}
		catch (MqttException e)
		{
			logger.warn("Could not unsubscribe from topic {}", topic, e);
		}
		finally
		{
			this.subscriptionsLock.unlock();
		}
	}

	public void disconnect()
	{
		if (!this.isConnected())
			return;
		try
		{
			this.client.disconnectForcibly();
		}
		catch (MqttException e)
		{
			logger.warn("Could not correctly disconnect client.", e);
		}
	}


	@Override
	public void publish(Topic topic, Map<String, String> topicParameters, byte[] payload, int qos, boolean retain, int messageId) throws MqttException
	{
		topic = this.config.getTopicBase().resolve(topic);
		MqttMessage toPublish = new MqttMessage();
		if (messageId > 0)
			toPublish.setId(messageId);
		toPublish.setPayload(payload);
		toPublish.setQos(qos);
		toPublish.setRetained(retain);
		this.client.publish(topic.fillTopic(topicParameters), toPublish);
		logger.debug("{}({},{}) {}", topic.fillTopic(topicParameters), qos, retain, new String(payload, StandardCharsets.UTF_8));
	}

}

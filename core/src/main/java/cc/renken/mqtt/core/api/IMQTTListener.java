package cc.renken.mqtt.core.api;

@FunctionalInterface
public interface IMQTTListener
{
	
	public void handleMessage(IMQTTRecvMessage message);
}
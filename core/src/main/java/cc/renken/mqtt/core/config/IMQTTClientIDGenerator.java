/**
 * 
 */
package cc.renken.mqtt.core.config;

/**
 * @author renkenh
 *
 */
@FunctionalInterface
public interface IMQTTClientIDGenerator<C extends AConfigurationBase>
{
	public String generate(C config);
}

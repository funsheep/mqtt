package cc.renken.mqtt.core.impl;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.mqtt.core.api.IMQTTClient;
import cc.renken.mqtt.core.api.IMQTTService;
import cc.renken.mqtt.core.config.ClientConfiguration;
import cc.renken.mqtt.core.config.ServiceConfiguration;

public abstract class AMQTTService implements IMQTTService
{

	protected static final Logger logger = LoggerFactory.getLogger(AMQTTService.class);


	private final MQTTClientFactory factory = new MQTTClientFactory();


	MQTTClientFactory factory()
	{
		return this.factory;
	}

	@Override
	public IMQTTClient getClient(ClientConfiguration clientConfig) throws MqttException
	{
		return this.factory().getClient(clientConfig);
	}

	@Override
	public abstract ServiceConfiguration getConfiguration();

	@Override
	public void shutdown()
	{
		this.factory.shutdown();
	}

}

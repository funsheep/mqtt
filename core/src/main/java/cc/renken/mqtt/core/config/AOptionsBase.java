/**
 * 
 */
package cc.renken.mqtt.core.config;

import java.time.Duration;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttClientPersistence;

/**
 * @author renkenh
 *
 */
public abstract class AOptionsBase<C extends AConfigurationBase> extends AConfigValueContainer
{

	protected AOptionsBase()
	{
		//do nothing
	}
	
	protected AOptionsBase(AOptionsBase<C> options)
	{
		super(options);
	}

	@Override
	public void setAutomaticReconnect(boolean enabled)
	{
		super.setAutomaticReconnect(enabled);
	}

	@Override
	public void setCleanSession(boolean startClean)
	{
		super.setCleanSession(startClean);
	}

	@Override
	public void setConnectionTimeout(Duration duration)
	{
		super.setConnectionTimeout(duration);
	}

	@Override
	public void setCustomWebSocketHeaders(Map<String, String> customHeaders)
	{
		super.setCustomWebSocketHeaders(customHeaders);
	}

	@Override
	public void setKeepAliveInterval(Duration duration)
	{
		super.setKeepAliveInterval(duration);
	}

	@Override
	public void setMaxInflight(int maxInflight)
	{
		super.setMaxInflight(maxInflight);
	}

	@Override
	public void setMaxReconnectDelay(Duration duration)
	{
		super.setMaxReconnectDelay(duration);
	}

	@Override
	public void setMqttVersion(MQTTVersion version)
	{
		super.setMqttVersion(version);
	}

	@Override
	public void setQoS(int qos)
	{
		super.setQoS(qos);
	}

	@Override
	public void setRetain(boolean retain)
	{
		super.setRetain(retain);
	}

	/**
	 * @param persitence the persitence to set
	 */
	@Override
	public void setPersistence(MqttClientPersistence persitence)
	{
		super.setPersistence(persitence);
	}

	public void setClientIDGenerator(IMQTTClientIDGenerator<C> generator)
	{
		super.clientIDGenerator(generator);
	}
}

/**
 * 
 */
package cc.renken.mqtt.core.impl;

/**
 * @author renkenh
 *
 */
class RefCount
{

	private int count = 0;


	public void inc()
	{
		this.count++;
	}

	public int incGet()
	{
		return this.count++;
	}
	
	public int get()
	{
		return this.count;
	}

	public void dec()
	{
		this.count--;
	}

	public int decGet()
	{
		return this.count--;
	}
}

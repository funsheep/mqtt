/**
 * 
 */
package cc.renken.mqtt.core.api;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renkenh
 *
 */
public class Topic
{
	
	public final class Parameter
	{
		private final TopicPart part;
		
		private Parameter(TopicPart part)
		{
			this.part = part;
		}
		
		public String getName()
		{
			return this.part.value;
		}
		
		public boolean isPath()
		{
			return this.part.type == PType.NAMED_PATH;
		}
		
		public boolean isSublevel()
		{
			return this.part.type == PType.NAMED_SUB;
		}
		
		public Topic getTopic()
		{
			return Topic.this;
		}
		
		public int hashCode()
		{
			return this.part.value.hashCode() + this.part.type.hashCode();
		}
		
		public boolean equals(Object o)
		{
			if (!(o instanceof Parameter))
				return false;
			Parameter other = (Parameter) o;
			if (!this.getTopic().equals(other.getTopic()))
				return false;
			if (!this.getName().equals(other.getName()))
				return false;
			return this.part.type == other.part.type;
		}
	}
	
	public abstract static class AValue
	{
		private final Parameter parameter;
		
		private AValue(Parameter card)
		{
			this.parameter = card;
		}
		
		public final Parameter getParameter()
		{
			return this.parameter;
		}
		
		public abstract String getValue();
	}
	
	public static final class SublevelValue extends AValue
	{
		private final String value;
		
		public SublevelValue(String value, Parameter card)
		{
			super(card);
			if (!card.isSublevel())
				throw new IllegalArgumentException("Only sublevel wildcards for sublevel parameter allowed.");
			this.value = value;
		}

		@Override
		public String getValue()
		{
			return this.value;
		}
	}
	
	public static final class PathValue extends AValue
	{
		private final List<String> value;
		
		public PathValue(String[] value, Parameter card)
		{
			this(Arrays.asList(value), card);
		}

		public PathValue(List<String> value, Parameter card)
		{
			super(card);
			if (!card.isPath())
				throw new IllegalArgumentException("Only path wildcard for path parameter allowed.");
			this.value = Collections.unmodifiableList(new ArrayList<>(value));
		}

		@Override
		public String getValue()
		{
			return this.value.stream().collect(Collectors.joining(Topics.SEPARATOR));
		}
		
		public List<String> getValues()
		{
			return this.value;
		}
	}
	

	private enum PType
	{
		FIXED,
		NAMED_SUB,
		NAMED_PATH,
		SUB,
		PATH
	}
	
	private static class TopicPart
	{
		private final PType type;
		private final String value;
		
		private TopicPart(PType type, String value)
		{
			this.type = type;
			this.value = value;
		}
		
		@Override
		public int hashCode()
		{
			return this.value.hashCode();
		}
		
		@Override
		public boolean equals(Object other)
		{
			if (!(other instanceof TopicPart))
				return false;
			TopicPart part = (TopicPart) other;
			return part.type == this.type && part.value.equals(this.value);
		}
	}


	private final TopicPart[] parts;
	private final int namedCount;
	private List<Parameter> parameters;
	private String mqtt;
	

	/**
	 * 
	 */
	Topic(List<String> topics)
	{
		this.parts = extractParts(topics);
		this.namedCount = countNamedParameters(this.parts);
	}

	private Topic(TopicPart[] parts)
	{
		this.parts = Arrays.stream(parts).filter((tp) -> tp.value.length() > 0).toArray(TopicPart[]::new);
		this.namedCount = countNamedParameters(parts);
	}
	
	private static final int countNamedParameters(TopicPart[] parts)
	{
		return (int) Arrays.stream(parts).filter((p) -> p.type == PType.NAMED_SUB || p.type == PType.NAMED_PATH).count();
	}
	
	public boolean matches(String topic)
	{
		String[] toMatch = normalize(topic).split("\\/");	//i know, takes resources, however, less complex
		
		if (toMatch.length < this.parts.length - (this.hasPath() ? 1 : 0))
			return false;

		int index = 0;
		for (TopicPart p : this.parts)
		{
			switch (p.type)
			{
				case FIXED:
					if (!toMatch[index].equals(p.value))
						return false;
					break;
				case NAMED_SUB:
				case SUB:
					break;
				case NAMED_PATH:
				case PATH:
					return true;
				default:
					break;
			}
			index++;
		}
		return true;
	}

	public boolean hasParameters()
	{
		return Arrays.stream(this.parts).filter((p) -> p.type != PType.FIXED).findAny().isPresent();
	}
	
	public boolean hasNamedParameters()
	{
		return this.hasNamedPath() || Arrays.stream(this.parts).filter((p) -> p.type == PType.NAMED_SUB).findAny().isPresent();
	}
	
	public List<Parameter> getNamedParameters()
	{
		if (this.parameters == null)
			this.parameters = Arrays.stream(this.parts).filter((p) -> p.type == PType.NAMED_SUB || p.type == PType.NAMED_PATH).map((p) -> new Parameter(p)).collect(Collectors.toUnmodifiableList());
		return this.parameters;
	}
	
	public boolean hasPath()
	{
		return this.hasNamedPath() || this.parts.length > 0 && this.parts[this.parts.length-1].type == PType.PATH;
	}
	
	public boolean hasNamedPath()
	{
		return this.parts.length > 0 && this.parts[this.parts.length-1].type == PType.NAMED_PATH;
	}
	
	public Parameter getNamedPath()
	{
		if (!this.hasNamedPath())
			return null;
		return new Parameter(this.parts[this.parts.length-1]);
	}
	
	public Topic resolve(Topic child)
	{
		//FIXME can create topics with several parameters having the same name
		int toCopy = this.parts.length - (this.hasPath() ? 1 : 0);
		TopicPart[] parts = new TopicPart[toCopy + child.parts.length];
		for (int i = 0; i < toCopy; i++)
			parts[i] = this.parts[i];
		for (int i = 0; i < child.parts.length; i++)
			parts[toCopy + i] = child.parts[i];
		return new Topic(parts);
	}
	
	public Deque<AValue> extractParameterValues(String topic)
	{
		String[] toMatch = normalize(topic).split("\\/");	//i know, takes resources, however, less complex
		
		if (toMatch.length < this.parts.length - (this.hasPath() ? 1 : 0))
			throw new IllegalArgumentException("Given topic does not match needed length.");

		Deque<AValue> result = new ArrayDeque<>(this.namedCount);
		int index = 0;
		for (TopicPart p : this.parts)
		{
			switch (p.type)
			{
				case FIXED:
					if (!toMatch[index].equals(p.value))
						throw new IllegalArgumentException("Given topic does not match this topic.");
					break;
				case NAMED_SUB:
					result.add(new SublevelValue(toMatch[index], new Parameter(p)));
					break;
				case SUB:
				default:
					break;
				case NAMED_PATH:
					result.add(new PathValue(Arrays.copyOfRange(toMatch, index, toMatch.length), new Parameter(p)));
				case PATH:
					return result;
			}
			index++;
		}
		return result;
	}
	
	public String fillTopic(String... parameterValues)
	{
		return this.fillTopic(Arrays.asList(parameterValues));
	}

	public String fillTopic(List<String> parameterValues)
	{
		if (parameterValues.size() < this.namedCount - (this.hasNamedPath() ? 1 : 0))
			throw new IllegalArgumentException("Given parameter value list is too small for number of named wildcards in this topic.");
		
		Deque<String> list = new ArrayDeque<>(parameterValues);
		Deque<String> result = new ArrayDeque<>(this.parts.length);
		for (TopicPart p : this.parts)
		{
			switch (p.type)
			{
				case FIXED:
					result.add(p.value);
					break;
				case NAMED_PATH:
				case PATH:
					result.addAll(list);
					list.clear();
					break;
				case NAMED_SUB:
				case SUB:
					result.add(list.removeFirst());
					break;
			}
		}
		return result.stream().collect(Collectors.joining("/"));
	}

	public String fillTopic(Map<String, String> parameterValues)
	{
		List<String> values = new ArrayList<>(this.namedCount+2);
		List<Parameter> params = this.getNamedParameters();
		for (Parameter param : params)
			values.add(parameterValues.entrySet().stream().filter((e) -> e.getKey().equals(param.getName())).findAny().get().getValue());
		
		Parameter path = this.getNamedPath();
		if (path != null)
			parameterValues.entrySet().stream().filter((e) -> e.getKey().equals(path.getName())).findAny().ifPresent((e) -> values.add(e.getValue()));
		return this.fillTopic(values);
	}

	public String fillTopic(AValue... parameterValues)
	{
		return this.fillTopic(Arrays.asList(parameterValues));
	}

	public String fillTopic(Collection<AValue> parameterValues)
	{
		List<String> values = new ArrayList<>(this.namedCount+2);
		List<Parameter> params = this.getNamedParameters();
		for (Parameter param : params)
			values.add(parameterValues.stream().filter((pv) -> pv.getParameter().equals(param)).map((v) -> (SublevelValue) v).findAny().get().getValue());
		
		Parameter path = this.getNamedPath();
		if (path != null)
			parameterValues.stream().filter((pv) -> pv.getParameter().equals(path)).map((v) -> (PathValue) v).findAny().ifPresent((pv) -> values.addAll(pv.getValues()));
		return this.fillTopic(values);
	}
	
	public String toMQTT()
	{
		if (this.mqtt == null)
		{
			final Function<TopicPart, String> mapper = (p) ->
			{
				switch (p.type)
				{
					case FIXED:
						return p.value;
					case NAMED_PATH:
					case PATH:
						return "#";
					case NAMED_SUB:
					case SUB:
					default:
						return "+";
				}
			};
			this.mqtt = Arrays.stream(this.parts).map(mapper).collect(Collectors.joining("/"));
		}
		return this.mqtt;
	}
	
	private static final TopicPart[] extractParts(List<String> topicDefinitions)
	{
		List<TopicPart> parts = new ArrayList<>(topicDefinitions.size()*2);
		for (String definition : topicDefinitions)
		{
			String[] split = normalize(definition).split("\\/");
			for (String s : split)
			{
				PType type = PType.FIXED;
				String value = s;
				if (value.length() == 0)
					continue;
				if (s.startsWith("+"))
				{
					if (s.length() == 1)
					{
						type = PType.SUB;
						value = null;
					}
					else
					{
						type = PType.NAMED_SUB;
						value = s.substring(1);
					}
				}
				else if (s.startsWith("#"))
				{
					if (s.length() == 1)
					{
						type = PType.NAMED_SUB;
						value = null;
					}
					else
					{
						type = PType.NAMED_PATH;
						value = s.substring(1);
					}
				}
				parts.add(new TopicPart(type, value));
			}
		}
		return parts.toArray(new TopicPart[parts.size()]);
	}
	
	private static final String normalize(String topic)
	{
		if (topic.length() == 0)
			return Topics.SEPARATOR;
		
		int start = topic.startsWith("/") ? 1 : 0;
		int cut = topic.endsWith("/") ? 1 : 0;
		if (start+cut == 0)
			return topic;

		if (topic.length()-cut <= 0)
			return Topics.SEPARATOR;
		return topic.substring(start, topic.length()-cut);
	}


	@Override
	public int hashCode()
	{
		int code = 1;
		for (TopicPart part : this.parts)
			code = code * 31 + part.hashCode();
		return code;
	}

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Topic))
			return false;
		Topic other = (Topic) o;
		if (other.parts.length != this.parts.length)
			return false;
		for (int i = this.parts.length-1; i >= 0; i--)
			if (!this.parts[i].equals(other.parts[i]))
				return false;
		return true;
	}

}

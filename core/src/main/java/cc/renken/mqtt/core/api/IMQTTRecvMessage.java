/**
 * 
 */
package cc.renken.mqtt.core.api;

import java.util.Collection;

import cc.renken.mqtt.core.api.Topic.AValue;

/**
 * @author renkenh
 *
 */
public interface IMQTTRecvMessage
{

	public String getMQTTTopic();
	
	public Collection<AValue> getTopicParameters();

	public AValue getTopicParameter(String name);

	public int getQoS();
	
	public boolean isRetained();
	
	public int getId();

	public byte[] getPayload();

	public default boolean hasPayload()
	{
		return this.getPayload() != null;
	}

	public boolean maybeDuplicate();
}

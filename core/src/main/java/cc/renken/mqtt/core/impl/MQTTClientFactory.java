/**
 * 
 */
package cc.renken.mqtt.core.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import cc.renken.mqtt.core.api.Topics;
import cc.renken.mqtt.core.config.AConfigurationBase;
import cc.renken.mqtt.core.config.ClientConfiguration;
import cc.renken.mqtt.core.config.ClientOptions;
import cc.renken.mqtt.core.config.ServiceConfiguration;

/**
 * @author renkenh
 *
 */
class MQTTClientFactory
{
	
//	private static final Logger logger = LoggerFactory.getLogger(MQTTClientFactory.class);
//
	private final Map<String, MQTTClient> clientsByConfigID = new HashMap<>();
	private final Map<String, RefCount> clientRefsByConfigID = new HashMap<>();


	public synchronized MQTTClient getClient(ServiceConfiguration config) throws MqttException
	{
		return this.getClient(new ClientConfiguration(new ClientOptions(), config));
	}
	
	public synchronized MQTTClient getClient(ClientConfiguration config) throws MqttException
	{
		MQTTClient client = this.clientsByConfigID.get(config.getID());
		RefCount count = this.clientRefsByConfigID.get(config.getID());
		try
		{
			if (client != null)
				return client;
	
			IMqttClient mqttClient = new MqttClient(config.getServerURIs()[0], config.getClientIDGenerator().generate(config), config.getPersistence());
			client = new MQTTClient(config, mqttClient);
			this.clientsByConfigID.put(config.getID(), client);
			count = new RefCount();
			this.clientRefsByConfigID.put(config.getID(), count);
		}
		finally
		{
			count.inc();
		}
		return client;
	}

	public synchronized void returnClient(AConfigurationBase config)
	{
		String configID = config.getID();
		RefCount count = this.clientRefsByConfigID.get(configID);
		if (count.decGet() <= 0)
		{
			this.clientRefsByConfigID.remove(configID);
			MQTTClient client = this.clientsByConfigID.remove(configID);
			client.disconnect();
		}
		//disconnect/shutdown client when there is no topic left. Remove after certain timeout (no reusage)
	}

	synchronized void shutdown()
	{
		this.clientRefsByConfigID.clear();
		Collection<MQTTClient> clients = new ArrayList<>(this.clientsByConfigID.values());
		this.clientsByConfigID.clear();
		for (MQTTClient client : clients)
			client.disconnect();
			
	}

	static MqttConnectOptions createOptions(AConfigurationBase config)
	{
		MqttConnectOptions options = new MqttConnectOptions();
		options.setAutomaticReconnect(config.getAutomaticReconnect());
		options.setCleanSession(config.getCleanSession());
		options.setConnectionTimeout((int) config.getConnectionTimeout().toSeconds());
		options.setCustomWebSocketHeaders(config.getCustomWebSocketHeaders());
		options.setHttpsHostnameVerificationEnabled(config.getHttpsHostnameVerificationEnabled());
		options.setKeepAliveInterval((int) config.getKeepAliveInterval().toSeconds());
		options.setMaxInflight(config.getMaxInflight());
		options.setMaxReconnectDelay((int) config.getMaxReconnectDelay().toMillis());
		options.setMqttVersion(config.getMqttVersion().pahoID);
		if (config.getPassword() != null)
			options.setPassword(config.getPassword().toCharArray());
		options.setServerURIs(config.getServerURIs());
		options.setSocketFactory(config.getSocketFactory());
		options.setSSLHostnameVerifier(config.getSSLHostnameVerifier());
		options.setSSLProperties(config.getSSLClientProperties());
		options.setUserName(config.getUserName());
		return options;
	}

	static MqttConnectOptions createOptions(ClientConfiguration config)
	{
		MqttConnectOptions options = createOptions((AConfigurationBase) config);
		if (config.getLastWill() != null && config.getLastWillDestination() != null)
			options.setWill(config.getTopicBase().resolve(Topics.get(config.getLastWillDestination())).toMQTT(), config.getLastWill(), config.getQoS(), config.getRetain());
		return options;
	}

}

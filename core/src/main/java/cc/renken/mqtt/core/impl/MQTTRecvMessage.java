/**
 * 
 */
package cc.renken.mqtt.core.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import cc.renken.mqtt.core.api.IMQTTRecvMessage;
import cc.renken.mqtt.core.api.Topic;
import cc.renken.mqtt.core.api.Topic.AValue;

/**
 * @author renkenh
 *
 */
class MQTTRecvMessage implements IMQTTRecvMessage
{

	private final Topic topic;
	private final String mqttTopic;
	private final MqttMessage message;
	private Map<String, AValue> topicParameters;


	/**
	 * 
	 */
	public MQTTRecvMessage(String mqttTopic, Topic topic, MqttMessage message)
	{
		this.mqttTopic = mqttTopic;
		this.topic = topic;
		this.message = message;
	}

	@Override
	public int getQoS()
	{
		return this.message.getQos();
	}

	@Override
	public boolean isRetained()
	{
		return this.message.isRetained();
	}

	@Override
	public byte[] getPayload()
	{
		return this.message.getPayload();
	}

	@Override
	public boolean maybeDuplicate()
	{
		return this.message.isDuplicate();
	}

	@Override
	public String getMQTTTopic()
	{
		return this.mqttTopic;
	}

	@Override
	public Collection<AValue> getTopicParameters()
	{
		if (this.topicParameters == null)
		{
			this.topicParameters = new HashMap<>();
			Deque<AValue> values = this.topic.extractParameterValues(this.mqttTopic);
			for (AValue v : values)
				this.topicParameters.put(v.getParameter().getName(), v);
		}
		return Collections.unmodifiableCollection(this.topicParameters.values());
	}

	@Override
	public int getId()
	{
		return this.message.getId();
	}

	@Override
	public AValue getTopicParameter(String name)
	{
		this.getTopicParameters();
		return this.topicParameters.get(name);
	}

}

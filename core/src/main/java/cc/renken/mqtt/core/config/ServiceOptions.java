/**
 * 
 */
package cc.renken.mqtt.core.config;

import java.util.Map;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;

import cc.renken.mqtt.core.api.Topics;

/**
 * @author renkenh
 *
 */
public class ServiceOptions extends AOptionsBase<AConfigurationBase>
{
	
	public ServiceOptions()
	{
		//do nothing
	}
	
	public ServiceOptions(ServiceOptions options)
	{
		super(options);
	}

	public void setHttpsHostnameVerification(boolean enabled)
	{
		super.setHttpsHostnameVerification(enabled);
	}
	
	public void setSSLClientProperties(Map<String, String> sslProperties)
	{
		super.setSSLClientProperties(sslProperties);
	}

	public void setSSLHostnameVerifier(HostnameVerifier verifier)
	{
		super.setSSLHostnameVerifier(verifier);
	}

	public void setPassword(String password)
	{
		super.setPassword(password);
	}
	
	public void setServerURIs(String[] uris)
	{
		super.setServerURIs(uris);
	}
	
	public void setSocketFactory(SocketFactory factory)
	{
		super.setSocketFactory(factory);
	}
	
	public void setUserName(String username)
	{
		super.setUserName(username);
	}

	public void setTopicBase(String topicBase)
	{
		super.setTopicBase(Topics.get(topicBase));
	}

}

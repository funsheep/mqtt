/**
 * 
 */
package cc.renken.mqtt.core.config;

import org.eclipse.paho.client.mqttv3.internal.websocket.Base64;

/**
 * @author renkenh
 *
 */
public class ClientConfiguration extends AConfigurationBase
{

	private final ServiceConfiguration serviceConfig;


	/**
	 * 
	 */
	public ClientConfiguration(ClientConfiguration config)
	{
		super(config);
		this.serviceConfig = new ServiceConfiguration(config.serviceConfig);
	}

	/**
	 * 
	 */
	public ClientConfiguration(ClientOptions clientOptions, ServiceConfiguration serviceConfig)
	{
		super(clientOptions);
		this.serviceConfig = serviceConfig;
	}


	protected void createStringRepresentation(StringBuilder sb)
	{
		super.createStringRepresentation(sb);
		byte[] lastWill = this.getLastWill();
		if (lastWill != null)
		{
			sb.append(this.getLastWillDestination());
			sb.append(Base64.encodeBytes(lastWill));
		}
	}


	/**
	 * @return the lastWillDestination
	 */
	@Override
	public String getLastWillDestination()
	{
		return super.getLastWillDestination();
	}

	/**
	 * @return the lastWill
	 */
	@Override
	public byte[] getLastWill()
	{
		return super.getLastWill();
	}
	
	@Override
	protected ServiceConfiguration defaultConfiguration()
	{
		return this.serviceConfig;
	}

}

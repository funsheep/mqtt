/**
 * 
 */
package cc.renken.mqtt.core.config;

import cc.renken.mqtt.core.api.Topic;

/**
 * @author renkenh
 *
 */
public class ClientOptions extends AOptionsBase<ClientConfiguration>
{

	public ClientOptions()
	{
		//do nothing
	}
	
	public ClientOptions(ClientOptions options)
	{
		super(options);
	}

	/**
	 * @param lastWillDestination the lastWillDestination to set
	 */
	public void setLastWillDestination(String lastWillDestination)
	{
		super.setLastWillDestination(lastWillDestination);
	}

	/**
	 * @param lastWill the lastWill to set
	 */
	public void setLastWill(byte[] lastWill)
	{
		super.setLastWill(lastWill);
	}

	public void setTopicBase(Topic topicBase)
	{
		super.setTopicBase(topicBase);
	}
}

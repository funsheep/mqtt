/**
 * 
 */
package cc.renken.mqtt.core.api;

import java.util.Collections;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttException;

import cc.renken.mqtt.core.config.ClientConfiguration;

/**
 * @author renkenh
 *
 */
public interface IMQTTClient
{

	public default void publish(String topic, byte[] payload) throws MqttException
	{
		this.publish(topic, payload, this.getConfig().getQoS(), this.getConfig().getRetain());
	}

	public default void publish(String topic, byte[] payload, int qos, boolean retain) throws MqttException
	{
		this.publish(topic, payload, qos, retain, 0);
	}

	public default void publish(String topic, byte[] payload, int qos, boolean retain, int messageId) throws MqttException
	{
		this.publish(Topics.get(topic), Collections.emptyMap(), payload, qos, retain, messageId);
	}

	public default void publish(Topic topic, Map<String, String> topicParameters, byte[] payload) throws MqttException
	{
		this.publish(topic, topicParameters, payload, this.getConfig().getQoS(), this.getConfig().getRetain());
	}

	public default void publish(Topic topic, Map<String, String> topicParameters, byte[] payload, int qos, boolean retain) throws MqttException
	{
		this.publish(topic, topicParameters, payload, qos, retain, 0);
	}

	public void publish(Topic topic, Map<String, String> topicParameters, byte[] payload, int qos, boolean retain, int messageId) throws MqttException;


	public default void subscribe(String topic, IMQTTListener listener) throws MqttException
	{
		this.subscribe(Topics.get(topic), listener);
	}

	public void subscribe(Topic topic, IMQTTListener listener) throws MqttException;

	public default void unsubscribe(String topic, IMQTTListener listener)
	{
		this.unsubscribe(Topics.get(topic), listener);
	}

	public void unsubscribe(Topic topic, IMQTTListener listener);

	
	public void connect() throws MqttException;
	
	public boolean isConnected();
	
	public void disconnect();
	
	public ClientConfiguration getConfig();
}

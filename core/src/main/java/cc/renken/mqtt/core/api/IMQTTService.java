package cc.renken.mqtt.core.api;

import org.eclipse.paho.client.mqttv3.MqttException;

import cc.renken.mqtt.core.config.ClientConfiguration;
import cc.renken.mqtt.core.config.ClientOptions;
import cc.renken.mqtt.core.config.ServiceConfiguration;

public interface IMQTTService
{

	public default IMQTTClient getClient(ClientOptions options) throws MqttException
	{
		return this.getClient(new ClientConfiguration(options, this.getConfiguration()));
	}
	
	public IMQTTClient getClient(ClientConfiguration options) throws MqttException;
	
	public ServiceConfiguration getConfiguration();

	public void shutdown();

}
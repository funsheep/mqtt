/**
 * 
 */
package cc.renken.mqtt.core.config;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

/**
 * @author renkenh
 *
 */
public enum MQTTVersion
{

	DEFAULT(MqttConnectOptions.MQTT_VERSION_DEFAULT),
	V3_1(MqttConnectOptions.MQTT_VERSION_3_1),
	V3_1_1(MqttConnectOptions.MQTT_VERSION_3_1_1);
	
	
	public final int pahoID;
	
	private MQTTVersion(int pahoID)
	{
		this.pahoID = pahoID;
	}

}

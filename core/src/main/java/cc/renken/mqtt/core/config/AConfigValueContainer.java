/**
 * 
 */
package cc.renken.mqtt.core.config;

import java.time.Duration;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.hibernate.validator.constraints.time.DurationMin;

import cc.renken.mqtt.core.api.Topic;

/**
 * @author renkenh
 *
 */
public abstract class AConfigValueContainer
{
	
	@Valid
	@DurationMin
	private Duration keepAliveInterval = null;
	@Valid
	@Min(0)
	private Integer maxInflight = null;
	private String userName = null;
	private String password = null;

	private SocketFactory socketFactory = null;
	private Properties sslClientProps = null;
	private Boolean httpsHostnameVerificationEnabled = null;

	private HostnameVerifier sslHostnameVerifier = null;
	private Boolean cleanSession = null;
	@Valid
	@DurationMin
	private Duration connectionTimeout = null;
	@Valid
	@Size(min=1)
	private String[] serverURIs = null;
	private MQTTVersion mqttVersion = null;
	private Boolean automaticReconnect = null;
	@Valid
	@DurationMin
	private Duration maxReconnectDelay = null;
	private Properties customWebSocketHeaders = null;
	@Valid
	@Min(0)
	@Max(2)
	private Integer qos = null;
	private Boolean retain = null;

	private MqttClientPersistence persistence = null;
	private Topic topicBase;
	
	private String lastWillDestination;
	
	private byte[] lastWill;
	
	private IMQTTClientIDGenerator<AConfigurationBase> clientIDGenerator;


	protected AConfigValueContainer()
	{
		//no further setup
	}
	
	protected AConfigValueContainer(AConfigValueContainer copy)
	{
		setup(this::setAutomaticReconnect, copy::automaticReconnect);
		setup(this::setCleanSession, copy::cleanSession);
		setup(this::setHttpsHostnameVerification, copy::httpsHostnameVerification);
		setup(this::setMaxInflight, copy::maxInflight);
		setup(this::setMqttVersion, copy::getMqttVersion);
		setup(this::setPassword, copy::getPassword);
		setup(this::setQoS, copy::qos);
		setup(this::setRetain, copy::retain);
		setup(this::setServerURIs, copy::getServerURIs);
		setup(this::setTopicBase, copy::getTopicBase);
		setup(this::setUserName, copy::getUserName);
		setup(this::setLastWill, copy::getLastWill);
		setup(this::setLastWillDestination, copy::getLastWillDestination);
		setup(this::setKeepAliveInterval, copy::getKeepAliveInterval);
		setup(this::setMaxReconnectDelay, copy::getMaxReconnectDelay);
		setup(this::setConnectionTimeout, copy::getConnectionTimeout);

		setupProps(this::setSSLClientProperties, copy::getSSLClientProperties);
		setupProps(this::setCustomWebSocketHeaders, copy::getCustomWebSocketHeaders);

		setup(this::setPersistence, copy::getPersistence);
		setup(this::setSocketFactory, copy::getSocketFactory);
		setup(this::setSSLHostnameVerifier, copy::getSSLHostnameVerifier);
		setup(this::clientIDGenerator, copy::getClientIDGenerator);
	}
	
	protected static <T> void setup(Consumer<T> consumer, Supplier<T> callable)
	{
		T value = callable.get();
		if (value != null)
			consumer.accept(value);
	}

	protected static void setupProps(Consumer<Map<String, String>> consumer, Supplier<Properties> callable)
	{
		Properties value = callable.get();
		if (value != null)
		{
			@SuppressWarnings({ "rawtypes", "unchecked" })
			Map<String, String> mapped = (Map) value;
			consumer.accept(mapped);
		}
	}


	protected Topic getTopicBase()
	{
		return this.topicBase;
	}

	protected void setTopicBase(Topic topicBase)
	{
		this.topicBase = topicBase;
	}

	/**
	 * @return the keepAliveInterval
	 */
	protected Duration getKeepAliveInterval()
	{
		return this.keepAliveInterval;
	}
	
	/**
	 * @param keepAliveInterval the keepAliveInterval to set
	 */
	protected void setKeepAliveInterval(Duration duration)
	{
		this.keepAliveInterval = duration;
	}
	/**
	 * @return the maxInflight
	 */
	protected Integer maxInflight()
	{
		return maxInflight;
	}
	/**
	 * @param maxInflight the maxInflight to set
	 */
	protected void setMaxInflight(int maxInflight)
	{
		this.maxInflight = maxInflight;
	}
	/**
	 * @return the userName
	 */
	protected String getUserName()
	{
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	protected void setUserName(String userName)
	{
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	protected String getPassword()
	{
		return password;
	}
	/**
	 * @param password the password to set
	 */
	protected void setPassword(String password)
	{
		this.password = password;
	}
	/**
	 * @return the socketFactory
	 */
	protected SocketFactory getSocketFactory()
	{
		return this.socketFactory;
	}
	/**
	 * @param socketFactory the socketFactory to set
	 */
	protected void setSocketFactory(SocketFactory socketFactory)
	{
		this.socketFactory = socketFactory;
	}
	
	/**
	 * @return the sslClientProps
	 */
	protected Properties getSSLClientProperties()
	{
		return sslClientProps;
	}
	/**
	 * @param sslClientProps the sslClientProps to set
	 */
	protected void setSSLClientProperties(Map<String, String> sslClientProps)
	{
		this.sslClientProps = new Properties();
		this.sslClientProps.putAll(sslClientProps);
	}
	/**
	 * @return the httpsHostnameVerificationEnabled
	 */
	protected Boolean httpsHostnameVerification()
	{
		return httpsHostnameVerificationEnabled;
	}
	/**
	 * @param httpsHostnameVerificationEnabled the httpsHostnameVerificationEnabled to set
	 */
	protected void setHttpsHostnameVerification(boolean enabled)
	{
		this.httpsHostnameVerificationEnabled = enabled;
	}
	/**
	 * @return the sslHostnameVerifier
	 */
	protected HostnameVerifier getSSLHostnameVerifier()
	{
		return this.sslHostnameVerifier;
	}
	
	/**
	 * @param sslHostnameVerifier the sslHostnameVerifier to set
	 */
	protected void setSSLHostnameVerifier(HostnameVerifier sslHostnameVerifier)
	{
		this.sslHostnameVerifier = sslHostnameVerifier;
	}
	/**
	 * @return the cleanSession
	 */
	protected Boolean cleanSession()
	{
		return cleanSession;
	}
	/**
	 * @param cleanSession the cleanSession to set
	 */
	protected void setCleanSession(boolean cleanSession)
	{
		this.cleanSession = cleanSession;
	}
	/**
	 * @return the connectionTimeout
	 */
	protected Duration getConnectionTimeout()
	{
		return this.connectionTimeout;
	}
	/**
	 * @param connectionTimeout the connectionTimeout to set
	 */
	protected void setConnectionTimeout(Duration duration)
	{
		this.connectionTimeout = duration;
	}
	/**
	 * @return the serverURIs
	 */
	protected String[] getServerURIs()
	{
		if (this.serverURIs == null)
			return null;
		String[] copy = new String[this.serverURIs.length];
		System.arraycopy(this.serverURIs, 0, copy, 0, this.serverURIs.length);
		return copy;
	}
	/**
	 * @param serverURIs the serverURIs to set
	 */
	protected void setServerURIs(String[] serverURIs)
	{
		if (serverURIs == null)
		{
			this.serverURIs = null;
			return;
		}
		this.serverURIs = new String[serverURIs.length];
		System.arraycopy(serverURIs, 0, this.serverURIs, 0, serverURIs.length);
	}
	/**
	 * @return the mqttVersion
	 */
	protected MQTTVersion getMqttVersion()
	{
		return mqttVersion;
	}
	/**
	 * @param mqttVersion the mqttVersion to set
	 */
	protected void setMqttVersion(MQTTVersion mqttVersion)
	{
		this.mqttVersion = mqttVersion;
	}
	/**
	 * @return the automaticReconnect
	 */
	protected Boolean automaticReconnect()
	{
		return automaticReconnect;
	}
	/**
	 * @param automaticReconnect the automaticReconnect to set
	 */
	protected void setAutomaticReconnect(boolean automaticReconnect)
	{
		this.automaticReconnect = automaticReconnect;
	}
	/**
	 * @return the maxReconnectDelay
	 */
	protected Duration getMaxReconnectDelay()
	{
		return this.maxReconnectDelay;
	}
	/**
	 * @param maxReconnectDelay the maxReconnectDelay to set
	 */
	protected void setMaxReconnectDelay(Duration duration)
	{
		this.maxReconnectDelay = duration;
	}
	/**
	 * @return the customWebSocketHeaders
	 */
	protected Properties getCustomWebSocketHeaders()
	{
		return customWebSocketHeaders;
	}
	/**
	 * @param customWebSocketHeaders the customWebSocketHeaders to set
	 */
	protected void setCustomWebSocketHeaders(Map<String, String> customWebSocketHeaders)
	{
		this.customWebSocketHeaders = new Properties();
		this.customWebSocketHeaders.putAll(customWebSocketHeaders);
	}
	/**
	 * @return the qos
	 */
	protected Integer qos()
	{
		return qos;
	}
	/**
	 * @param qos the qos to set
	 */
	protected void setQoS(int qos)
	{
		this.qos = qos;
	}
	/**
	 * @return the retain
	 */
	protected Boolean retain()
	{
		return retain;
	}
	/**
	 * @param retain the retain to set
	 */
	protected void setRetain(boolean retain)
	{
		this.retain = retain;
	}
	/**
	 * @return the persitence
	 */
	protected MqttClientPersistence getPersistence()
	{
		return persistence;
	}
	
	/**
	 * @param persitence the persitence to set
	 */
	protected void setPersistence(MqttClientPersistence persitence)
	{
		this.persistence = persitence;
	}
	
	/**
	 * @return the lastWillDestination
	 */
	protected String getLastWillDestination()
	{
		return lastWillDestination;
	}

	/**
	 * @param lastWillDestination the lastWillDestination to set
	 */
	protected void setLastWillDestination(String lastWillDestination)
	{
		this.lastWillDestination = lastWillDestination;
	}

	/**
	 * @return the lastWill
	 */
	protected byte[] getLastWill()
	{
		if (this.lastWill == null)
			return null;
		byte[] copy = new byte[this.lastWill.length];
		System.arraycopy(this.lastWill, 0, copy, 0, this.lastWill.length);
		return copy;
	}

	/**
	 * @param lastWill the lastWill to set
	 */
	protected void setLastWill(byte[] lastWill)
	{
		if (lastWill == null)
		{
			this.lastWill = null;
			return;
		}
		this.lastWill = new byte[lastWill.length];
		System.arraycopy(lastWill, 0, this.lastWill, 0, lastWill.length);
	}

	/**
	 * @return the clientIDGenerator
	 */
	protected IMQTTClientIDGenerator<AConfigurationBase> getClientIDGenerator()
	{
		return this.clientIDGenerator;
	}

	/**
	 * @param clientIDGenerator the clientIDGenerator to set
	 */
	@SuppressWarnings("unchecked")
	protected void clientIDGenerator(IMQTTClientIDGenerator<? extends AConfigurationBase> clientIDGenerator)
	{
		this.clientIDGenerator = (IMQTTClientIDGenerator<AConfigurationBase>) clientIDGenerator;
	}

}

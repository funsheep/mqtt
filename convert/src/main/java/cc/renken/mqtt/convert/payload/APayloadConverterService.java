/**
 * 
 */
package cc.renken.mqtt.convert.payload;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.IODirection;

/**
 * @author renkenh
 *
 */
public abstract class APayloadConverterService implements IPayloadConverterService
{
	
	private final Map<Class<?>, IPayloadConverter<?>> readCache = new ConcurrentHashMap<>();
	private final Map<Class<?>, IPayloadConverter<?>> writeCache = new ConcurrentHashMap<>();


	protected abstract List<IPayloadConverter<?>> getConverters(ContentType contentType);

	protected void invalidateCache()
	{
		this.writeCache.clear();
		this.readCache.clear();
	}


	@Override
	public boolean canRead(Class<?> clazz, ContentType contentType)
	{
		return this.findConverter(clazz, contentType, IODirection.READ) != null;
	}

	@Override
	public boolean canWrite(Class<?> clazz, ContentType contentType)
	{
		return this.findConverter(clazz, contentType, IODirection.WRITE) != null;
	}

	@Override
	public <T> T read(Class<? extends T> clazz, ContentType contentType, byte[] toRead) throws ConvertException
	{
		IPayloadConverter<T> converter = this.findConverter(clazz, contentType, IODirection.READ);
		if (converter != null)
			return converter.read(clazz, contentType, toRead);
		throw new ConvertException(clazz, contentType, IODirection.READ);
	}
	
	@Override
	public byte[] write(Object toWrite, ContentType contentType) throws ConvertException
	{
		IPayloadConverter<?> converter = this.findConverter(toWrite.getClass(), contentType, IODirection.WRITE);
		if (converter != null)
			return converter.write(convertUnchecked(toWrite), contentType);
		throw new ConvertException(toWrite.getClass(), contentType, IODirection.WRITE);
	}

	
	private <PAYLOAD> IPayloadConverter<PAYLOAD> findConverter(final Class<? extends PAYLOAD> payloadClass, final ContentType contentType, final IODirection direction)
	{
		IPayloadConverter<?> cached = this.writeCache.get(payloadClass);
		if (cached != null)
			return convertUnchecked(cached);

		final List<IPayloadConverter<?>> converters = this.getConverters(contentType);
		final Deque<Class<?>> toProcess = new LinkedList<>(Arrays.asList(payloadClass));

		while (!toProcess.isEmpty())
		{
			Class<?> current = toProcess.removeFirst();
			
			IPayloadConverter<?> found = converters.parallelStream().filter(this.generateFilter(current, contentType, direction)).findAny().orElse(null);
			if (found != null)
			{
				this.writeCache.putIfAbsent(payloadClass, found);
				return convertUnchecked(found);
			}
			
			if (current.getSuperclass() != null)
				toProcess.add(current.getSuperclass());
			toProcess.addAll(Arrays.asList(current.getInterfaces()));
		}

		return null;
	}
	
	private Predicate<? super IPayloadConverter<?>> generateFilter(Class<?> current, ContentType contentType, IODirection direction)
	{
		switch (direction)
		{
			case READ:
				return (c) -> c.canRead(current, contentType);
			case WRITE:
				return (c) -> c.canWrite(current, contentType);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private static final <T> T convertUnchecked(Object o)
	{
		return (T) o;
	}

}

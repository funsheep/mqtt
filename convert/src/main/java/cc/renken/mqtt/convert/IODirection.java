package cc.renken.mqtt.convert;

public enum IODirection
{
	READ,
	WRITE
}
/**
 * 
 */
package cc.renken.mqtt.convert.payload;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.IODirection;

/**
 * @author renkenh
 *
 */
public class DefaultSerializablePayloadConverter implements IPayloadConverter<Object>
{

	@Override
	public List<ContentType> supportedContentTypes()
	{
		return Arrays.asList(ContentType.SERIALIZED);
	}

	@Override
	public boolean canRead(Class<?> clazz, ContentType contentType)
	{
		return clazz instanceof Serializable && this.supportedContentTypes().contains(contentType);
	}

	@Override
	public boolean canWrite(Class<?> clazz, ContentType contentType)
	{
		return clazz instanceof Serializable && this.supportedContentTypes().contains(contentType);
	}

	@Override
	public Object read(Class<? extends Object> clazz, ContentType contentType, byte[] toRead) throws ConvertException
	{
		try
		{
			return (new ObjectInputStream(new ByteArrayInputStream(toRead))).readObject();
		}
		catch (ClassNotFoundException | IOException e)
		{
			throw new ConvertException(clazz, contentType, IODirection.READ, e);
		}
	}

	@Override
	public byte[] write(Object toWrite, ContentType contentType) throws ConvertException
	{
		try
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			(new ObjectOutputStream(out)).writeObject(toWrite);
			return out.toByteArray();
		}
		catch (IOException e)
		{
			throw new ConvertException(toWrite.getClass(), contentType, IODirection.WRITE, e);
		}
	}

}

/**
 * 
 */
package cc.renken.mqtt.convert.payload;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.IODirection;

/**
 * @author renkenh
 *
 */
public class EnumWriteConverter implements IPayloadConverter<Enum<?>>
{

	@Override
	public List<ContentType> supportedContentTypes()
	{
		return Arrays.asList(ContentType.TEXT);
	}

	@Override
	public boolean canRead(Class<?> clazz, ContentType contentType)
	{
		return contentType == ContentType.TEXT && clazz.isEnum();
	}

	public boolean canWrite(Class<?> clazz, ContentType contentType)
	{
		return contentType == ContentType.TEXT && clazz.isEnum();
	}

	@Override
	public Enum<?> read(Class<? extends Enum<?>> clazz, ContentType contentType, byte[] toRead) throws ConvertException
	{
		if (!this.canRead(clazz, contentType))
			throw new ConvertException(clazz, contentType, IODirection.READ);
		return Enum.valueOf(convert(clazz), new String(toRead, StandardCharsets.UTF_8));
	}

	@SuppressWarnings("unchecked")
	private static final <T> T convert(Object o)
	{
		return (T) o;
	}

	@Override
	public byte[] write(Enum<?> toWrite, ContentType contentType) throws ConvertException
	{
		if (contentType != ContentType.TEXT)
			throw new ConvertException(toWrite.getClass(), contentType, IODirection.WRITE);
		return toWrite.name().getBytes(StandardCharsets.UTF_8);
	}

}

/**
 * 
 */
package cc.renken.mqtt.convert.payload;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.IODirection;

/**
 * @author renkenh
 *
 */
public abstract class AJsonPayloadConverter implements IPayloadConverter<Object>
{

	protected abstract ObjectMapper getMapper();

	
	@Override
	public List<ContentType> supportedContentTypes()
	{
		return Arrays.asList(ContentType.JSON);
	}

	@Override
	public boolean canRead(Class<?> clazz, ContentType contentType)
	{
		return this.supportedContentTypes().contains(contentType) && this.getMapper().canDeserialize(TypeFactory.defaultInstance().constructType(clazz));
	}

	@Override
	public boolean canWrite(Class<?> clazz, ContentType contentType)
	{
		return this.supportedContentTypes().contains(contentType) && this.getMapper().canSerialize(clazz);
	}

	@Override
	public Object read(Class<? extends Object> clazz, ContentType contentType, byte[] toRead) throws ConvertException
	{
		try
		{
			return this.getMapper().readValue(toRead, clazz);
		}
		catch (IOException e)
		{
			throw new ConvertException(clazz, contentType, IODirection.READ);
		}
	}

	@Override
	public byte[] write(Object toWrite, ContentType contentType) throws ConvertException
	{
		try
		{
			return this.getMapper().writeValueAsBytes(toWrite);
		}
		catch (IOException e)
		{
			throw new ConvertException(toWrite.getClass(), contentType, IODirection.READ);
		}
	}

}

/**
 * 
 */
package cc.renken.mqtt.convert.payload;

import java.util.Arrays;
import java.util.List;

/**
 * @author renkenh
 *
 */
public class NoConverter implements IPayloadConverter<byte[]>
{

	@Override
	public List<ContentType> supportedContentTypes()
	{
		return Arrays.asList(ContentType.BINARY);
	}

	@Override
	public boolean canRead(Class<?> clazz, ContentType contentType)
	{
		return byte[].class.equals(clazz) && this.supportedContentTypes().contains(contentType);
	}

	@Override
	public boolean canWrite(Class<?> clazz, ContentType contentType)
	{
		return this.canRead(clazz, contentType);
	}

	@Override
	public byte[] read(Class<? extends byte[]> clazz, ContentType contentType, byte[] toRead)
	{
		return toRead;
	}

	@Override
	public byte[] write(byte[] toWrite, ContentType contentType)
	{
		return toWrite;
	}

}

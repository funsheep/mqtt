/**
 * 
 */
package cc.renken.mqtt.convert.payload;

import java.util.List;

import cc.renken.mqtt.convert.ConvertException;

/**
 * @author renkenh
 *
 */
public interface IPayloadConverter<T>
{

	public List<ContentType> supportedContentTypes();
	
	public default boolean canRead(Class<?> clazz, ContentType contentType)
	{
		return this.supportedContentTypes().contains(contentType);
	}

	public default boolean canWrite(Class<?> clazz, ContentType contentType)
	{
		return this.supportedContentTypes().contains(contentType);
	}

	
	public T read(Class<? extends T> clazz, ContentType contentType, byte[] toRead) throws ConvertException;
	
	public byte[] write(T toWrite, ContentType contentType) throws ConvertException;
}

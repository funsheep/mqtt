/**
 * 
 */
package cc.renken.mqtt.convert.param;

import cc.renken.mqtt.convert.ConvertException;

/**
 * @author renkenh
 *
 */
public interface IParameterConverter
{

	public boolean canConvert(Class<?> source, Class<?> target);

	public <T> T convert(Object value, Class<T> target) throws ConvertException;
}

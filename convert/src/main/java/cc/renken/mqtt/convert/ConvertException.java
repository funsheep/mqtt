/**
 * 
 */
package cc.renken.mqtt.convert;

import java.io.IOException;

import cc.renken.mqtt.convert.payload.ContentType;

/**
 * @author renkenh
 *
 */
public class ConvertException extends IOException
{
	
	private static final long serialVersionUID = -7331167263978015857L;

	
	private final Class<?> type;
	private final ContentType contentType;
	private final IODirection direction;

	/**
	 * 
	 */
	public ConvertException(Class<?> type, ContentType contentType, IODirection direction)
	{
		super(createMessage(type, contentType, direction));
		this.type = type;
		this.contentType = contentType;
		this.direction = direction;
	}


	/**
	 * @param message
	 * @param cause
	 */
	public ConvertException(Class<?> type, ContentType contentType, IODirection direction, Throwable cause)
	{
		super(createMessage(type, contentType, direction), cause);
		this.type = type;
		this.contentType = contentType;
		this.direction = direction;
	}


	/**
	 * @return the type
	 */
	public Class<?> getType()
	{
		return type;
	}

	/**
	 * @return the contentType
	 */
	public ContentType getContentType()
	{
		return contentType;
	}

	/**
	 * @return the direction
	 */
	public IODirection getDirection()
	{
		return direction;
	}


	private static final String createMessage(Class<?> type, ContentType contentType, IODirection direction)
	{
		return "Could not " + direction + " " + type + " for content type " + contentType;
	}
}

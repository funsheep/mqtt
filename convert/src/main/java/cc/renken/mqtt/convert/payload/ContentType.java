/**
 * 
 */
package cc.renken.mqtt.convert.payload;

/**
 * @author renkenh
 *
 */
public enum ContentType
{
	/** Custom serialization. */
	BINARY,
	/** Payload can be read/written by using java standard serialization. */
	SERIALIZED,
	/** Payload is read/written from/into JSON format. */
	JSON,
	/** Payload is read/written as plain UTF-8 text. */
	TEXT,
	/** Payload is read/written from/into XML format. */
	XML
}

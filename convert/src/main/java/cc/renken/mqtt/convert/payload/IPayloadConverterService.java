package cc.renken.mqtt.convert.payload;

import cc.renken.mqtt.convert.ConvertException;

public interface IPayloadConverterService
{

	public boolean canRead(Class<?> clazz, ContentType contentType);

	public boolean canWrite(Class<?> clazz, ContentType contentType);

	
	public <T> T read(Class<? extends T> clazz, ContentType contentType, byte[] toRead) throws ConvertException;
	
	public byte[] write(Object toWrite, ContentType contentType) throws ConvertException;

}
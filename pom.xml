<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    
	<properties>
		<paho.version>1.2.1</paho.version>
		<jackson.version>2.9.5</jackson.version>
		<spring-boot.version>2.1.7.RELEASE</spring-boot.version>
  		<mqtt.version>1.0.0-SNAPSHOT</mqtt.version>
		<annotation-api.version>1.3.2</annotation-api.version>
		<validate.version>1.0.1</validate.version>
	</properties>

    <parent>
    	<groupId>cc.renken</groupId>
    	<artifactId>parent</artifactId>
    	<version>1.2.0</version>
    </parent>

    <!-- project coordinates -->
    <artifactId>mqtt</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>

	<modules>
    	<module>spring</module>
    	<module>core</module>
    	<module>convert</module>
	</modules>

	<name>${project.groupId}:${project.artifactId}</name>
	<description>A simple mqtt library - based on paho - that provides an easy spring integration, like the rest-mvc and jpa repository patterns in standard spring.</description>
	<url>https://gitlab.com/funsheep/spring-mqtt</url>

	<licenses>
		<license>
			<name>Mozilla Public License Version 2.0</name>
			<url>https://www.mozilla.org/media/MPL/2.0/index.txt</url>
		</license>
	</licenses>
	<issueManagement>
		<url>https://gitlab.com/funsheep/mqtt/issues</url>
		<system>Gitlab</system>
	</issueManagement>
	<scm>
		<connection>scm:git:git://gitlab.com/funsheep/mqtt.git</connection>
		<developerConnection>scm:git:https://gitlab.com/funsheep/mqtt.git</developerConnection>
		<url>https://gitlab.com/funsheep/mqtt/tree/develop</url>
	</scm>
	<developers>
		<developer>
			<name>Hendrik Renken</name>
			<email>hendrik@renken.cc</email>
			<organization>cc.renken</organization>
			<organizationUrl>https://gitlab.com/funsheep/</organizationUrl>
		</developer>
	</developers>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>cc.renken</groupId>
				<artifactId>mqtt</artifactId>
				<version>${mqtt.version}</version>
			</dependency>
			<dependency>
				<groupId>cc.renken</groupId>
				<artifactId>mqtt-core</artifactId>
				<version>${mqtt.version}</version>
			</dependency>
			<dependency>
				<groupId>cc.renken</groupId>
				<artifactId>mqtt-convert</artifactId>
				<version>${mqtt.version}</version>
			</dependency>
			<dependency>
				<groupId>cc.renken</groupId>
				<artifactId>mqtt-spring</artifactId>
				<version>${mqtt.version}</version>
			</dependency>
		
			<dependency>
				<groupId>cc.renken</groupId>
	    		<artifactId>validate</artifactId>
	    		<version>${validate.version}</version>
			</dependency>

			<dependency>
	    	    <groupId>org.eclipse.paho</groupId>
	        	<artifactId>org.eclipse.paho.client.mqttv3</artifactId>
	        	<version>${paho.version}</version>
	    	</dependency>
	
			<dependency>
			    <groupId>com.fasterxml.jackson.core</groupId>
			    <artifactId>jackson-databind</artifactId>
	    		<version>${jackson.version}</version>
	    	</dependency>
	    	
	    	<dependency>
			    <groupId>com.fasterxml.jackson.module</groupId>
	    		<artifactId>jackson-module-parameter-names</artifactId>
	    		<version>${jackson.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<version>${spring-boot.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			
			<dependency>
			    <groupId>javax.annotation</groupId>
			    <artifactId>javax.annotation-api</artifactId>
			    <version>${annotation-api.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
	<dependencies>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>com.github.koraktor</groupId>
				<artifactId>mavanagaiata</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
			</plugin>
 			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
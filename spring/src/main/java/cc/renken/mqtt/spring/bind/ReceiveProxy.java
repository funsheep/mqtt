/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.util.StringUtils;

import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.api.IMQTTClient;
import cc.renken.mqtt.core.api.Topics;
import cc.renken.mqtt.spring.MQTT;
import cc.renken.mqtt.spring.MQTT.TopicMapping;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;
import cc.renken.mqtt.spring.config.SpringClientOptions;

/**
 * @author renkenh
 *
 */
public class ReceiveProxy
{

	private final IMQTTClient client;
	private final Collection<ReceiveMethodAdapter> adapters = new ArrayList<>();


	/**
	 * 
	 */
	public ReceiveProxy(Object receiver, ReceiveMethodAdapterFactory factory, ISpringMQTTService service) throws MqttException
	{
		SpringClientConfiguration config = new SpringClientConfiguration(createOptionsFrom(receiver.getClass()), service.getConfiguration());
		this.client = service.getClient(config);
		for (Method m : receiver.getClass().getMethods())
		{
			ReceiveMethodAdapter adapter = factory.generateAdapter(m, receiver, config);
			if (adapter != null)
				this.adapters.add(adapter);
		}
		this.client.connect();
		for (ReceiveMethodAdapter adapter : this.adapters)
			this.client.subscribe(adapter.getTopic(), adapter);
	}

	private static final SpringClientOptions createOptionsFrom(Class<?> clazz)
	{
		SpringClientOptions options = new SpringClientOptions();
		MQTT.TopicMapping mapping = clazz.getAnnotation(MQTT.TopicMapping.class);
		if (mapping != null && StringUtils.hasText(mapping.value()))
			options.setTopicBase(Topics.get(mapping.value()));
		if (mapping != null && StringUtils.hasText(mapping.contentType()))
			options.setContentType(ContentType.valueOf(mapping.contentType()));
		if (mapping != null && mapping.qos() != TopicMapping.QOS_NOT_SET)
			options.setQoS(mapping.qos());
		return options;
	}

}

/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.core.convert.support.DefaultConversionService;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.payload.IPayloadConverterService;
import cc.renken.mqtt.core.api.Topic.Parameter;
import cc.renken.mqtt.spring.MQTT.Param.Type;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;

/**
 * @author renkenh
 *
 */
class PublishMethodAdapter
{

	private final Method method;
	private final IPayloadConverterService converterService;

	private final ParameterDefinition paramDef;
	private final MethodConfiguration config;


	/**
	 * 
	 */
	public PublishMethodAdapter(Method method, IPayloadConverterService converterService, SpringClientConfiguration clientConfig)
	{
		this.method = method;
		this.converterService = converterService;
		
		this.paramDef = new ParameterDefinition(method);
		this.config	= new MethodConfiguration(method, clientConfig);
	}

	public boolean throwsMqttException()
	{
		return Arrays.stream(this.method.getExceptionTypes()).filter((t) -> t.equals(MqttException.class)).findAny().isPresent();
	}

	public final String computeTopic(Object[] callArgs)
	{
		if (!this.config.getTopic().hasNamedParameters())
			return this.config.getTopic().toMQTT();

		List<String> topicValues = new ArrayList<>(this.config.getTopic().getNamedParameters().size());
		for (Parameter topicParameter : this.config.getTopic().getNamedParameters())
		{
			String value = this.convertArgument(callArgs, topicParameter.getName());
			topicValues.add(value);
		}
		return this.config.getTopic().fillTopic(topicValues);
	}
	
	public final byte[] computeBody(Object[] callArgs) throws IllegalArgumentException, ConvertException
	{
		Object body = callArgs[this.paramDef.getParameter(Type.PAYLOAD).index];
		if (!this.converterService.canWrite(body.getClass(), this.config.getDefaultContentType()))
			throw new IllegalArgumentException("Could not find converter for body type " + body.getClass() + " and content type " + this.config.getDefaultContentType());
		return this.converterService.write(body, this.config.getDefaultContentType());
	}
	
	public final int computeQoS(Object[] callArgs)
	{
		cc.renken.mqtt.spring.bind.ParameterDefinition.Parameter qosParam = this.paramDef.getParameter(Type.QoS);
		if (qosParam != null)
			convertArgument(callArgs, qosParam);
		return this.config.getQoS();
	}

	public final boolean computeRetain(Object[] callArgs)
	{
		cc.renken.mqtt.spring.bind.ParameterDefinition.Parameter qosParam = this.paramDef.getParameter(Type.RETAIN);
		if (qosParam != null)
			convertArgument(callArgs, qosParam);
		return this.config.getRetain();
	}

	public final int computeMSGId(Object[] callArgs)
	{
		cc.renken.mqtt.spring.bind.ParameterDefinition.Parameter qosParam = this.paramDef.getParameter(Type.MSG_ID);
		if (qosParam != null)
			convertArgument(callArgs, qosParam);
		return 0;
	}

	private final <T> T convertArgument(Object[] callArgs, String paramName)
	{
		return convertArgument(callArgs, this.paramDef.getParameter(paramName));
	}

	@SuppressWarnings("unchecked")
	private static final <T> T convertArgument(Object[] callArgs, cc.renken.mqtt.spring.bind.ParameterDefinition.Parameter parameter)
	{
		Class<?> targetClass = null;
		switch (parameter.type)
		{
			case QoS:
			case MSG_ID:
				targetClass = int.class;
				break;
			case RETAIN:
				targetClass = boolean.class;
				break;
			case TOPIC_PARAM:
				targetClass = String.class;
				break;
			default:
				throw new IllegalArgumentException("Type " + parameter.type + " is not allowed for pushing.");
		}
		
		Object value = callArgs[parameter.index];
		if (!DefaultConversionService.getSharedInstance().canConvert(value.getClass(), targetClass))
			throw new IllegalArgumentException("Could not find converter for argument type " + value.getClass() + " and target " + targetClass);
		return (T) DefaultConversionService.getSharedInstance().convert(value, targetClass);
	}
	
}

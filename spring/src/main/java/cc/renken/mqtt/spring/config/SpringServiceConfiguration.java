/**
 * 
 */
package cc.renken.mqtt.spring.config;

import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.config.ServiceConfiguration;

/**
 * @author renkenh
 *
 */
public class SpringServiceConfiguration extends ServiceConfiguration
{

	private ContentType contentType;

	
	/**
	 * @param clientOptions
	 * @param serviceConfig
	 */
	public SpringServiceConfiguration(SpringServiceOptions serviceOptions)
	{
		super(serviceOptions);
		this.contentType = serviceOptions.getContentType();
	}
	
	/**
	 * @param config
	 */
	public SpringServiceConfiguration(SpringServiceConfiguration config)
	{
		super(config);
		this.contentType = config.contentType;
	}


	public ContentType getDefaultContentType()
	{
		if (this.contentType != null)
			return this.contentType;
		return ContentType.JSON;
	}

	public String getLastWillDestination()
	{
		return super.getLastWillDestination();
	}

	public byte[] getLastWill()
	{
		return super.getLastWill();
	}

}

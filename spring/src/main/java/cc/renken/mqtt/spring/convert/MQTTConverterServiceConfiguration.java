/**
 * 
 */
package cc.renken.mqtt.spring.convert;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cc.renken.mqtt.convert.payload.DefaultSerializablePayloadConverter;
import cc.renken.mqtt.convert.payload.EnumWriteConverter;
import cc.renken.mqtt.convert.payload.IPayloadConverter;
import cc.renken.mqtt.convert.payload.NoConverter;

/**
 * @author renkenh
 *
 */
@Configuration
public class MQTTConverterServiceConfiguration
{

	@Bean
	public IPayloadConverter<?> getSerializer()
	{
		return new DefaultSerializablePayloadConverter();
	}

	@Bean
	public IPayloadConverter<?> getNoConverter()
	{
		return new NoConverter();
	}

	@Bean
	public IPayloadConverter<?> getEnumConverter()
	{
		return new EnumWriteConverter();
	}
}

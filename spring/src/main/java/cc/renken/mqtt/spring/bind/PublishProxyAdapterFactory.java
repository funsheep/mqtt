/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.payload.IPayloadConverterService;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;


/**
 * @author renkenh
 *
 */
@Component
final class PublishProxyAdapterFactory
{

	public static final Pattern TOPIC_PATTERN = Pattern.compile("^(?:withQoS(?<qos1>\\d))?publishTo(?<topic>\\w+?)(?:withQoS(?<qos2>\\d))?$");

	
	@Autowired
	private IPayloadConverterService converterService;


	public final PublishMethodAdapter generateAdapter(Method method, SpringClientConfiguration config) throws ConvertException, MqttException
	{
		if (!Validation.validatePublishMethod(method))
			return null;
		return new PublishMethodAdapter(method, this.converterService, config);
	}
	
}

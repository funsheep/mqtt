/**
 * 
 */
package cc.renken.mqtt.spring.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.convert.payload.IPayloadConverterService;
import cc.renken.mqtt.spring.MQTT;

/**
 * @author renkenh
 *
 */
@Component
public class LastWillUtil
{
	
	@Autowired
	private IPayloadConverterService converterService;


	public final byte[] computeLastWill(MQTT.Publisher publisher, MQTT.TopicMapping mapping, ContentType defaultContentType) throws IllegalArgumentException, ConvertException
	{
		if (mapping != null && StringUtils.hasText(mapping.contentType()))
			defaultContentType = ContentType.valueOf(mapping.contentType());
		return computeLastWill(publisher.lastWill(), defaultContentType, this.converterService);
	}

	public static final byte[] computeLastWill(String lastWill, ContentType contentType, IPayloadConverterService convertService) throws IllegalArgumentException, ConvertException
	{
		if (!StringUtils.hasText(lastWill))
			return null;

		final int indexDD = lastWill.indexOf(':');
		if (indexDD < 0)
			throw new IllegalArgumentException("Incorrect format. Must be (base64|java|string):<content>");
		
		String defType = lastWill.substring(0, indexDD);
		String defValue = lastWill.substring(indexDD+1);
		switch (defType)
		{
			case "base64":
				return Base64.getDecoder().decode(defValue.getBytes(StandardCharsets.UTF_8));
			case "java":
				return callJavaFactory(defValue, contentType, convertService);
			case "string":
				return defValue.getBytes(StandardCharsets.UTF_8);
			default:
				throw new IllegalArgumentException("Unknown last will type "+defType+" for "+lastWill);
		}
	}

	private static final byte[] callJavaFactory(String factory, ContentType contentType, IPayloadConverterService convertService) throws IllegalArgumentException, ConvertException
	{
		String[] split = factory.split("#");
		if (split.length != 2)
			throw new IllegalArgumentException("Incorrect format. Must be java:<classname>#<methodname>");
		try
		{
			Class<?> clazz = Class.forName(split[0]);
			Method method = clazz.getMethod(split[1]);
			if (byte[].class.equals(method.getReturnType()))
				return (byte[]) method.invoke(null);
			if (Void.class.equals(method.getReturnType()))
				throw new IllegalArgumentException("Specified method " + factory + " is not a factory method.");
			if (convertService.canWrite(method.getReturnType(), contentType))
				return convertService.write(method.invoke(null), contentType);
			throw new IllegalArgumentException("Could not find converter for type " + method.getReturnType() + " and content type " + contentType);
		}
		catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException | ClassNotFoundException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

}

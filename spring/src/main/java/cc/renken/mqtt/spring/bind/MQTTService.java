/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import javax.validation.ValidatorFactory;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.renken.mqtt.core.api.IMQTTClient;
import cc.renken.mqtt.core.config.ClientOptions;
import cc.renken.mqtt.core.impl.AMQTTService;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;
import cc.renken.mqtt.spring.config.SpringClientOptions;
import cc.renken.mqtt.spring.config.SpringServiceConfiguration;
import cc.renken.mqtt.spring.config.ServiceConfigurationProperties;

/**
 * @author renkenh
 *
 */
@Component
public class MQTTService extends AMQTTService implements ISpringMQTTService
{

	private final SpringServiceConfiguration mqttConfig;

	@Autowired
	MQTTService(ValidatorFactory validators, ServiceConfigurationProperties configuration)
	{
		validators.getValidator().validate(configuration);
		this.mqttConfig = new SpringServiceConfiguration(configuration.getServiceOptions());
	}

	public IMQTTClient getClient(ClientOptions options) throws MqttException
	{
		return this.getClient(new SpringClientConfiguration(new SpringClientOptions(options), this.getConfiguration()));
	}

	@Override
	public SpringServiceConfiguration getConfiguration()
	{
		return this.mqttConfig;
	}

	//FIXME lifecycle: correct shutdown
}

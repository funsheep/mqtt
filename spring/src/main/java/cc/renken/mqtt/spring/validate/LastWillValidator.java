/**
 * 
 */
package cc.renken.mqtt.spring.validate;

import java.util.Base64;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

/**
 * LWT definition
 * * base64:
 * * java:<classname>#<methodname>
 * * string:
 * @author renkenh
 */
public class LastWillValidator implements ConstraintValidator<LastWill, String>
{

	@Override
	public final void initialize(LastWill constraintAnnotation)
	{
		//do nothing - no information in annotation
	}

	@Override
	public final boolean isValid(String value, ConstraintValidatorContext context)
	{
		if (value == null)
			return true;
		
		int dd = value.indexOf(':');
		if (dd < 0)
			return false;
	
		String type = value.substring(0, dd);
		value = value.substring(dd+1);

		switch (type)
		{
			case "base64":
				Base64.getDecoder().decode(value);
				return true;
			case "java":
				String[] split = value.split("#");
				if (split.length != 2)
					return false;
				try
				{
					Class<?> clazz = Class.forName(split[0]);
					clazz.getMethod(split[1]);
					return true;
				}
				catch (NoSuchMethodException | SecurityException | ClassNotFoundException e)
				{
					return false;
				}
			case "string":
				return StringUtils.hasText(value);
			default:
				return false;
		}
	}

}

/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Proxy;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.spring.util.LastWillUtil;

/**
 * @author renkenh
 *
 */
public class PublishProxyFactoryBean
{

	@Autowired
	private PublishProxyAdapterFactory factory;
	@Autowired
	private LastWillUtil lastWillUtil;
	@Autowired
	private ISpringMQTTService mqttService;


    @SuppressWarnings("unchecked")
    public <PUBLISHER> PUBLISHER createPublishProxyBean(ClassLoader classLoader, Class<PUBLISHER> clazz)
    {
        try
		{
    		if (!clazz.isInterface())
    			throw new RuntimeException("Proxy can be created for interfaces only.");
			return (PUBLISHER) Proxy.newProxyInstance(classLoader, new Class[] {clazz}, new PublishProxy((Class<?>) clazz, this.factory, this.lastWillUtil, this.mqttService));
		}
		catch (MqttException | ConvertException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }

}

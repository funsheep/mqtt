/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.MultiValueMap;

import cc.renken.mqtt.spring.MQTT;

/**
 * @author renkenh
 *
 */
@Configuration
public class PublishConfiguration implements ImportBeanDefinitionRegistrar, BeanClassLoaderAware, BeanFactoryAware
{

	private final ClassPathScanner classpathScanner = new ClassPathScanner(false);
	{
		this.classpathScanner.addIncludeFilter(new AnnotationTypeFilter(MQTT.Publisher.class));
	}
	private ClassLoader classLoader;
	private BeanFactory beanFactory;

	@Override
	public void setBeanClassLoader(ClassLoader classLoader)
	{
		this.classLoader = classLoader;
	}

	@Bean(name = "publishProxyBeanFactory")
	public PublishProxyFactoryBean getPublisherFactory()
	{
		return new PublishProxyFactoryBean();
	}

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry)
    {
    	List<String> basePackages = Collections.emptyList();
    	try
    	{
    		basePackages = AutoConfigurationPackages.get(this.beanFactory);
    	}
    	catch (IllegalStateException ex)
    	{
    		basePackages = Arrays.asList(getBasePackages(importingClassMetadata));
    	}
        for (String basePackage : basePackages)
            registerPublisherProxyFactory(basePackage, registry);
    }

    private static final String[] getBasePackages(AnnotationMetadata importingClassMetadata)
    {
        String[] scanPackages = null;
        MultiValueMap<String, Object> allAnnotationAttributes = importingClassMetadata.getAllAnnotationAttributes(ComponentScan.class.getName());
        if (allAnnotationAttributes != null)
        	scanPackages = (String[]) allAnnotationAttributes.getFirst("basePackages");

        String[] appPackages = null;
        allAnnotationAttributes = importingClassMetadata.getAllAnnotationAttributes(SpringBootApplication.class.getName());
        if (allAnnotationAttributes != null)
            appPackages = (String[]) allAnnotationAttributes.getFirst("scanBasePackages");

        String[] packages = Arrays.asList(scanPackages, appPackages).stream().filter(a -> a != null).flatMap(a -> Arrays.stream(a)).toArray(String[]::new);
        
        if (packages.length == 0)
        	packages = new String[] { extractMainPackage(importingClassMetadata) };
        
        return packages;
    }
    
    private static final String extractMainPackage(AnnotationMetadata importingClassMetadata)
    {
    	String classname = importingClassMetadata.getClassName();
    	int lastIndex = -1;
    	int indexDot = classname.indexOf('.');
    	while (indexDot > lastIndex)
    	{
    		lastIndex = indexDot;
    		indexDot = classname.indexOf('.', indexDot+1);
    	}
    	if (lastIndex <= 0)
    		return "";
    	return classname.substring(0, lastIndex);
    }

    private void registerPublisherProxyFactory(String basePackage, BeanDefinitionRegistry registry)
    {
    	try
    	{
    		for (BeanDefinition beanDefinition : this.classpathScanner.findCandidateComponents(basePackage))
    		{
    			final Class<?> clazz = Class.forName(beanDefinition.getBeanClassName());
    			final String beanName = ClassUtils.getShortNameAsProperty(clazz);

    			final GenericBeanDefinition proxyBeanDefinition = new GenericBeanDefinition();

    			ConstructorArgumentValues args = new ConstructorArgumentValues();
    			args.addGenericArgumentValue(this.classLoader);
    			args.addGenericArgumentValue(clazz);

    			proxyBeanDefinition.setBeanClass(clazz);
    			proxyBeanDefinition.setConstructorArgumentValues(args);
    			proxyBeanDefinition.setAutowireCandidate(true);
    			proxyBeanDefinition.setFactoryBeanName("publishProxyBeanFactory");
    			proxyBeanDefinition.setFactoryMethodName("createPublishProxyBean");

    			registry.registerBeanDefinition(beanName, proxyBeanDefinition);
    		}
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}

    }

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException
	{
		this.beanFactory = beanFactory;
	}

}

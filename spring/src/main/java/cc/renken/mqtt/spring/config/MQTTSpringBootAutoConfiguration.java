/**
 * 
 */
package cc.renken.mqtt.spring.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import cc.renken.mqtt.spring.bind.PublishConfiguration;
import cc.renken.mqtt.spring.bind.ReceiveConfiguration;
import cc.renken.mqtt.spring.convert.MQTTConverterServiceConfiguration;

/**
 * @author renkenh
 *
 */
@EnableConfigurationProperties(ServiceConfigurationProperties.class)
@Import({ ReceiveConfiguration.class, PublishConfiguration.class, MQTTConverterServiceConfiguration.class })
public class MQTTSpringBootAutoConfiguration
{
}

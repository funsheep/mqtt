/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.renken.mqtt.convert.payload.IPayloadConverterService;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;


/**
 * @author renkenh
 *
 */
@Component
final class ReceiveMethodAdapterFactory
{

	public static final Pattern TOPIC_PATTERN = Pattern.compile("^(?:withQoS(?<qos1>\\d))?receiveFrom(?<topic>\\w+?)(?:withQoS(?<qos2>\\d))?$");

	
	@Autowired
	private IPayloadConverterService converterService;

	
	public final ReceiveMethodAdapter generateAdapter(Method method, Object receiver, SpringClientConfiguration clientConfig) throws MqttException
	{
		if (!Validation.validateReceiveMethod(method))
			return null;
		return new ReceiveMethodAdapter(method, receiver, this.converterService, clientConfig);
	}
	
}

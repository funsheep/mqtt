/**
 * 
 */
package cc.renken.mqtt.spring.config;

import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.Map;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.hibernate.validator.constraints.time.DurationMin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.convert.payload.IPayloadConverterService;
import cc.renken.mqtt.core.config.AConfigurationBase;
import cc.renken.mqtt.core.config.IMQTTClientIDGenerator;
import cc.renken.mqtt.core.config.MQTTVersion;
import cc.renken.mqtt.core.utils.HostnameClientIDGenerator;
import cc.renken.mqtt.spring.util.LastWillUtil;
import cc.renken.mqtt.spring.validate.MQTTTopic;
import cc.renken.validate.TypeReference;

/**
 * @author renkenh
 *
 */
@Validated
@ConfigurationProperties(prefix="mqtt")
public class ServiceConfigurationProperties
{
	
	private static final Logger logger = LoggerFactory.getLogger(ServiceConfigurationProperties.class);
	
	public static class Persistence
	{
		private cc.renken.mqtt.core.config.Persistence type;
		private MqttClientPersistence persistence;
		private String directory;
		
		public void setType(cc.renken.mqtt.core.config.Persistence type)
		{
			this.type = type;
		}

		public void setCustomClassReference(@TypeReference(value=MqttClientPersistence.class, mayBeNull=true) String customClassReference)
		{
			this.setCustomPersistence(instantiate(customClassReference));
		}
		
		public void setCustomPersistence(@TypeReference(value=MqttClientPersistence.class, mayBeNull=true) MqttClientPersistence persistence)
		{
			this.persistence = persistence;
		}
		
		public void setDirectory(String directory)
		{
			this.directory = directory;
		}
	}
	
	public static class ClientID
	{
		private IMQTTClientIDGenerator<AConfigurationBase> generator;
		private String prefix;
		private boolean hostname = false;
		
		private ClientID()
		{
			//nothing
		}
		
		private ClientID(ClientID client)
		{
			this.generator = client.generator;
			this.prefix = client.prefix;
			this.hostname = client.hostname;
		}
		
		public void setGenerator(@Valid @TypeReference(value=IMQTTClientIDGenerator.class, mayBeNull=true) String generatorReference)
		{
			this.setGenerator(ServiceConfigurationProperties.<IMQTTClientIDGenerator<AConfigurationBase>>instantiate(generatorReference));
		}

		/**
		 * @param generator the generator to set
		 */
		public void setGenerator(IMQTTClientIDGenerator<AConfigurationBase> generator)
		{
			this.generator = generator;
		}
		
		/**
		 * @param clientIDPrefix the clientIDPrefix to set
		 */
		public void setPrefix(String clientIDPrefix)
		{
			this.prefix = clientIDPrefix;
		}
		
		public void setHostname(boolean hostnamePrefix)
		{
			this.hostname = hostnamePrefix;
		}
	}
	
	public static class LastWill
	{
		private String destination;
		private String content;
		
		public void setDestination(@MQTTTopic String destination)
		{
			this.destination = destination;
		}
		
		public void setContent(@cc.renken.mqtt.spring.validate.LastWill String content)
		{
			this.content = content;
		}
	}


	private final SpringServiceOptions options = new SpringServiceOptions();
	private final Persistence persistence = new Persistence();
	private final ClientID clientID = new ClientID();
	private final LastWill lastWill = new LastWill();
	private ContentType defaultCT = null;


	@Autowired
	private IPayloadConverterService converterService;

	public void setKeepAliveInterval(@Valid @DurationMin Duration duration)
	{
		this.options.setKeepAliveInterval(duration);
	}

	public void setMaxInflight(@Valid @Min(0) int maxInflight)
	{
		this.options.setMaxInflight(maxInflight);
	}

	public void setUserName(String username)
	{
		this.options.setUserName(username);
	}

	public void setPassword(String password)
	{
		this.options.setPassword(password);
	}
	
	public void setSocketFactory(@Valid @TypeReference(value=SocketFactory.class, mayBeNull=true) String factoryReference)
	{
		this.setSocketFactory(ServiceConfigurationProperties.<SocketFactory>instantiate(factoryReference));
	}
	
	public void setSocketFactory(SocketFactory factory)
	{
		this.options.setSocketFactory(factory);
	}
	
	public void setSSLClientProperties(Map<String, String> sslProperties)
	{
		this.options.setSSLClientProperties(sslProperties);
	}

	public void setHttpsHostnameVerification(boolean enabled)
	{
		this.options.setHttpsHostnameVerification(enabled);
	}

	public void setSSLHostnameVerifier(@Valid @TypeReference(value=HostnameVerifier.class, mayBeNull=true) String verifierReference)
	{
		this.setSSLHostnameVerifier(ServiceConfigurationProperties.<HostnameVerifier>instantiate(verifierReference));
	}

	public void setSSLHostnameVerifier(HostnameVerifier verifier)
	{
		this.options.setSSLHostnameVerifier(verifier);
	}

	public void setCleanSession(boolean startClean)
	{
		this.options.setCleanSession(startClean);
	}

	public void setConnectionTimeout(@Valid @DurationMin Duration timeout)
	{
		this.options.setConnectionTimeout(timeout);
	}

	public void setServerURIs(@Valid @NotNull @Size(min=1) String[] uris)
	{
		this.options.setServerURIs(uris);
	}
	
	public void setMqttVersion(MQTTVersion version)
	{
		this.options.setMqttVersion(version);
	}

	public void setAutomaticReconnect(boolean enabled)
	{
		this.options.setAutomaticReconnect(enabled);
	}

	public void setCustomWebSocketHeaders(Map<String, String> customHeaders)
	{
		this.options.setCustomWebSocketHeaders(customHeaders);
	}

	public void setQoS(@Valid @Min(0) @Max(2) int qos)
	{
		this.options.setQoS(qos);
	}

	public void setRetain(boolean retain)
	{
		this.options.setRetain(retain);
	}

	public void setTopicBase(String topicBase)
	{
		topicBase = topicBase.replace("${host.name}", hostName());
		this.options.setTopicBase(topicBase);
	}
	
	public void setMaxReconnectDelay(@Valid @DurationMin Duration duration)
	{
		this.options.setMaxReconnectDelay(duration);
	}
	
	public void setDefaultContentType(ContentType contentType)
	{
		this.defaultCT = contentType;
	}
	
	public ClientID getClientID()
	{
		return this.clientID;
	}
	
	public Persistence getPersistence()
	{
		return this.persistence;
	}
	
	public LastWill getLastWill()
	{
		return this.lastWill;
	}
	
	private MqttClientPersistence createMqttPersistence()
	{
		if (this.persistence.type == cc.renken.mqtt.core.config.Persistence.IN_MEMORY)
			return new MemoryPersistence();
		else if (this.persistence.type == cc.renken.mqtt.core.config.Persistence.FILE)
		{
			if (this.persistence.directory != null)
				return new MqttDefaultFilePersistence(this.persistence.directory);
			return new MqttDefaultFilePersistence();
		}
		return this.persistence.persistence;
	}
	
	private IMQTTClientIDGenerator<AConfigurationBase> createClientIDGenerator()
	{
		if (!this.clientID.hostname && this.clientID.generator == null && !StringUtils.hasText(this.clientID.prefix))
			return null;
		
		final ClientID client = new ClientID(this.clientID);
		return (c) ->
		{
			String id = "";
			if (client.hostname)
				id += HostnameClientIDGenerator.getHostname();
			if (client.prefix != null)
				id += client.prefix;
			if (client.generator != null)
				id += client.generator.generate(c);
			else
				id += System.nanoTime();
			return id;
		};
	}
	
	
	public SpringServiceOptions getServiceOptions()
	{
		this.options.setPersistence(this.createMqttPersistence());
		this.options.setClientIDGenerator(this.createClientIDGenerator());
		this.options.setDefaultContentType(this.defaultCT);
		try
		{
			this.options.setLastWill(LastWillUtil.computeLastWill(this.lastWill.content, this.defaultCT != null ? this.defaultCT : ContentType.JSON, this.converterService));
		}
		catch (IllegalArgumentException | ConvertException e)
		{
			throw new RuntimeException(e);
		}
		this.options.setLastWillDestination(this.lastWill.destination);
		return new SpringServiceOptions(this.options);
	}
	
	private static final String hostName()
	{
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			try
			{
				return InetAddress.getLocalHost().getHostAddress();
			}
			catch (UnknownHostException e1)
			{
				return "unknown_host";
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private static final <T> T instantiate(String reference)
	{
		try
		{
			return (T) Class.forName(reference).getConstructor().newInstance();
		}
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e)
		{
			logger.error("Could not instantiate: {}", reference, e );
			throw new RuntimeException("Could not instantiate " + reference);
		}
	}
}

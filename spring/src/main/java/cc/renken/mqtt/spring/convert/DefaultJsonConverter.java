/**
 * 
 */
package cc.renken.mqtt.spring.convert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import cc.renken.mqtt.convert.payload.AJsonPayloadConverter;

/**
 * Utilize Spring-jackson auto-configuration.
 * @author renkenh
 */
@Component
public class DefaultJsonConverter extends AJsonPayloadConverter
{
	
	@Autowired
	private ObjectMapper mapper;

	@Override
	protected ObjectMapper getMapper()
	{
		return this.mapper;
	}

}

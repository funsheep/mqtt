/**
 * 
 */
package cc.renken.mqtt.spring.config;

import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.config.ClientOptions;

/**
 * @author renkenh
 *
 */
public class SpringClientOptions extends ClientOptions
{

	private ContentType contentType;


	/**
	 * 
	 */
	public SpringClientOptions()
	{
		//do nothing
	}

	public SpringClientOptions(ClientOptions options)
	{
		super(options);
	}

	public SpringClientOptions(SpringClientOptions options)
	{
		super(options);
		this.contentType = options.contentType;
	}


	public void setContentType(ContentType contentType)
	{
		this.contentType = contentType;
	}

	ContentType getContentType()
	{
		return contentType;
	}

}

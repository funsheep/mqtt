/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.api.Topic;
import cc.renken.mqtt.core.api.Topics;
import cc.renken.mqtt.spring.MQTT;
import cc.renken.mqtt.spring.MQTT.PublishOptions;
import cc.renken.mqtt.spring.MQTT.PublishOptions.Retain;
import cc.renken.mqtt.spring.MQTT.TopicMapping;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;

/**
 * @author renkenh
 *
 */
public class MethodConfiguration
{

	private final TopicMapping mapping;
	private final PublishOptions pOptions;
	private final SpringClientConfiguration defaultConfig;

	private final int methodNameQoS;
	private final Topic methodTopic;
	

	/**
	 * 
	 */
	public MethodConfiguration(Method method, SpringClientConfiguration clientConfig)
	{
		this.mapping = method.getAnnotation(TopicMapping.class);
		this.pOptions = method.getAnnotation(PublishOptions.class);
		this.defaultConfig = clientConfig;

		this.methodNameQoS = extractQoSFromName(method);
		this.methodTopic = computeTopic(method);
	}

	
	public Topic getTopic()
	{
		return this.methodTopic;
	}
	
	public int getQoS()
	{
		if (this.mapping != null && this.mapping.qos() != TopicMapping.QOS_NOT_SET)
			return this.mapping.qos();
		if (this.methodNameQoS != TopicMapping.QOS_NOT_SET)
			return this.methodNameQoS;
		return this.defaultConfig.getQoS();
	}
	
	public boolean getRetain()
	{
		if (this.pOptions != null && this.pOptions.retain() != Retain.UNDEFINED)
			return this.pOptions.retain() == Retain.TRUE;
		return this.defaultConfig.getRetain();
	}
	
	public ContentType getDefaultContentType()
	{
		if (this.mapping != null && StringUtils.hasText(this.mapping.contentType()))
			return ContentType.valueOf(this.mapping.contentType());
		return this.defaultConfig.getContentType();
	}

	private static final int extractQoSFromName(Method method)
	{
		Pattern topicPattern = null;
		if (method.getDeclaringClass().getAnnotation(MQTT.Publisher.class) != null)
			topicPattern = PublishProxyAdapterFactory.TOPIC_PATTERN;
		else if (method.getDeclaringClass().getAnnotation(MQTT.Receiver.class) != null)
			topicPattern = ReceiveMethodAdapterFactory.TOPIC_PATTERN;
		if (topicPattern == null)
			return MQTT.TopicMapping.QOS_NOT_SET;
		
		Matcher matcher = topicPattern.matcher(method.getName());
		if (matcher.find())
		{
			String qos1 = matcher.group("qos1");
			String qos2 = matcher.group("qos2");
			if (qos1 != null)
				return Character.getNumericValue(qos1.charAt(0));
			if (qos2 != null)
				return Character.getNumericValue(qos2.charAt(0));
		}
		return MQTT.TopicMapping.QOS_NOT_SET;
	}

	private static final Topic computeTopic(Method method)
	{
		Pattern topicPattern = null;
		if (method.getDeclaringClass().getAnnotation(MQTT.Publisher.class) != null)
			topicPattern = PublishProxyAdapterFactory.TOPIC_PATTERN;
		else if (method.getDeclaringClass().getAnnotation(MQTT.Receiver.class) != null)
			topicPattern = ReceiveMethodAdapterFactory.TOPIC_PATTERN;
		if (topicPattern == null)
			return null;

		MQTT.TopicMapping clazzMapping  = method.getDeclaringClass().getAnnotation(MQTT.TopicMapping.class);
		final String clazzTopic = clazzMapping != null ? clazzMapping.value() : null;

		MQTT.TopicMapping methodMapping = method.getAnnotation(MQTT.TopicMapping.class);
		if (methodMapping != null && StringUtils.hasText(methodMapping.value()))
			return Topics.get(clazzTopic, methodMapping.value());
		
		Matcher matcher = topicPattern.matcher(method.getName());
		if (matcher.find())
		{
			String topic = matcher.group("topic");
			if (StringUtils.hasText(topic))
				topic = topic.replace('_', '/');
			if (StringUtils.hasText(topic))
				return Topics.get(clazzTopic, topic);
		}
		return null;
	}
		
}

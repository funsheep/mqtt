package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import cc.renken.mqtt.spring.MQTT;
import cc.renken.mqtt.spring.MQTT.Param.Type;

class ParameterDefinition
{

	public static class Parameter
	{
		public final String name;
		public final int index;
		public final MQTT.Param.Type type;
		public final Class<?> valueType;
		public final boolean isRequired;
		
		Parameter(String name, int index, MQTT.Param.Type type, Class<?> valueType, boolean isRequired)
		{
			this.name = name;
			this.index = index;
			this.type = type;
			this.valueType = valueType;
			this.isRequired = isRequired;
		}
	}
	
	
	private final List<Parameter> parameters;

	
	public ParameterDefinition(Method method)
	{
		this.parameters = new ArrayList<>(ParameterFactory.extractParameters(method));
	}


	public Parameter getParameter(Type type)
	{
		if (type == Type.TOPIC_PARAM)
			return null;
		for (Parameter check : this.parameters)
			if (check.type == type)
				return check;
		return null;
	}
	
	public Parameter getParameter(String name)
	{
		for (Parameter check : this.parameters)
			if (check.name.equals(name))
				return check;
		return null;
	}

	public Parameter getParameter(int index)
	{
		return this.parameters.get(index);
	}
	
	public List<Parameter> getTopicParameters()
	{
		return this.parameters.parallelStream().filter((p) -> p.type == Type.TOPIC_PARAM).collect(Collectors.toList());
	}

	public List<Parameter> getParameters()
	{
		return Collections.unmodifiableList(this.parameters);
	}
	
	public int size()
	{
		return this.parameters.size();
	}

}
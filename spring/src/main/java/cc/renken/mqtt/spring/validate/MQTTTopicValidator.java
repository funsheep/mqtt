/**
 * 
 */
package cc.renken.mqtt.spring.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 * @author renkenh
 */
public class MQTTTopicValidator implements ConstraintValidator<MQTTTopic, String>
{
	
	@Override
	public final void initialize(MQTTTopic constraintAnnotation)
	{
		//do nothing - no information in annotation
	}

	@Override
	public final boolean isValid(String value, ConstraintValidatorContext context)
	{
		if (value == null)
			return true;

		return !value.contains("//");
	}

}

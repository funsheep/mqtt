package cc.renken.mqtt.spring.bind;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.support.DefaultConversionService;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.payload.IPayloadConverterService;
import cc.renken.mqtt.core.api.IMQTTListener;
import cc.renken.mqtt.core.api.IMQTTRecvMessage;
import cc.renken.mqtt.core.api.Topic;
import cc.renken.mqtt.core.api.Topic.AValue;
import cc.renken.mqtt.spring.MQTT.Param.Type;
import cc.renken.mqtt.spring.bind.ParameterDefinition.Parameter;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;

class ReceiveMethodAdapter implements IMQTTListener
{
	
	//to call
	private final Object receiver;
	private final Method method;
	
	private final ParameterDefinition paramDef;
	protected final IPayloadConverterService converterService;
	private final MethodConfiguration config;


	public ReceiveMethodAdapter(Method method, Object receiver, IPayloadConverterService converterService, SpringClientConfiguration clientConfig) throws MqttException
	{
		this.method = method;
		this.receiver = receiver;
		
		this.paramDef = new ParameterDefinition(method);
		this.converterService = converterService;
		this.config = new MethodConfiguration(method, clientConfig);
	}

	
	public Topic getTopic()
	{
		return this.config.getTopic();
	}

	@Override
	public void handleMessage(IMQTTRecvMessage message)
	{
		//if there is payload, but the method does not want it - might not be the correct one
		//if there is no payload, but the method wants it - it is not the correct one
		if (message.hasPayload() != (this.paramDef.getParameter(Type.PAYLOAD) != null))
			return;
		
		try
		{
			Object[] callParameters = this.extractCallParameters(message);
			if (callParameters == null)	//null = aborted
				return;
			this.method.invoke(this.receiver, callParameters);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			e.printStackTrace();//FIXME better exception handling
		}
	}

	//if the payload cannot be parsed into the defined type - it is not the correct one
	//if the method wants to have payload and there is none - it is not the correct one
	private Object[] extractCallParameters(IMQTTRecvMessage message)
	{
		Object[] callParameters = new Object[this.paramDef.size()];
		for (int i = 0; i < callParameters.length; i++)
		{
			Parameter param = this.paramDef.getParameter(i);
			switch (param.type)
			{
				case PAYLOAD:
					if (!message.hasPayload())
					{
						if (param.isRequired)
							return null;
						callParameters[i] = null;
						break;
					}
					if (!this.converterService.canRead(param.valueType, this.config.getDefaultContentType()))
						throw new UnsupportedOperationException("Could not convert body. No converter found for byte[] to " + this.config.getDefaultContentType());
					try
					{
						callParameters[i] = this.converterService.read(param.valueType, this.config.getDefaultContentType(), message.getPayload());
					}
					catch (ConvertException ex)
					{
						return null;
					}
					break;
				case TOPIC_PARAM:
					AValue topicValue = message.getTopicParameter(param.name);
					if (topicValue == null)
					{
						if (param.isRequired)
							return null;
						callParameters[i] = null;
						break;
					}
					try
					{
						callParameters[i] = DefaultConversionService.getSharedInstance().convert(topicValue.getValue(), param.valueType);
					}
					catch (ConversionException ex)
					{
						return null;
					}
					break;
				case QoS:
					callParameters[i] = message.getQoS();
					break;
				case TOPIC:
					callParameters[i] = message.getMQTTTopic();
					break;
				case MSG_ID:
					callParameters[i] = message.getId();
				default:
					throw new IllegalArgumentException("Parameter of type "+param.type+" not supported for receiving methods.");
			}
		}
		return callParameters;
	}
	
}
/**
 * 
 */
package cc.renken.mqtt.spring;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import cc.renken.mqtt.spring.config.MQTTSpringBootAutoConfiguration;

@Retention(SOURCE)
/**
 * @author renkenh
 *
 */
public @interface MQTT
{

	/**
	 * @author renkenh
	 */
	@Documented
	@Retention(RUNTIME)
	@Target(TYPE)
	@Import({ MQTTSpringBootAutoConfiguration.class })
	public @interface Enable {}
	
	/**
	 * Optional specify the name of the topic variable as defined in the topic specs. If not defined, the name of the parameter is used instead.
	 * @author renkenh
	 */
	@Documented
	@Retention(RUNTIME)
	@Target(PARAMETER)
	public @interface Param
	{
		
		public static final boolean DEFAULT_ISREQUIRED = true;

		
		public enum Type
		{
			TOPIC,
			TOPIC_PARAM,
			QoS,
			RETAIN,
			PAYLOAD,
			MSG_ID
		}

		public Type value();

		public String name() default "";
		
		public boolean isRequired() default DEFAULT_ISREQUIRED;
	}
	
	/**
	 * @author renkenh
	 *
	 */
	@Documented
	@Retention(RUNTIME)
	@Target(TYPE)
	//@Component
	public @interface Publisher
	{
		/**
		 * LWT definition - if any. Leave string empty for no LWT. Supports the following types:
		 * * base64:
		 * * java:
		 * * string:
		 * @return The lwt definition to use for the push topic.
		 */
		public String lastWill() default "";

		public String lastWillDestination() default "";

	}

	/**
	 * @author renkenh
	 *
	 */
	@Documented
	@Retention(RUNTIME)
	@Target(TYPE)
	@Component
	public @interface Receiver {}

	/**
	 * Use this to define the topic the annotated method should receive data for.
	 * The annotation can be used at two different locations:
	 * * Type - define an overall basis for all method topics.
	 * * Method - define that this method should be called and on which topic (prefixed with the basis topic, if any).
	 * 
	 * The overall topic must follow the rules of the MQTT protocol. However, here some syntax flavor is added to allow the
	 * retrieval of wildcard data.
	 * To define a topic variable - the same mechanism as for path variables defined in rest controllers - one uses the following
	 * syntax: '{+:abc}' to define a + variable (see MQTT specs for meaning) with the name 'abc'. This name then can be used together
	 * with {@link MQTTParam} to inject the actual value on the method call.
	 * The same goes for '#' wildcards in mqtt topics: For this use '{#:abc}'.
	 * 
	 * If the parameter in the method call is missing for a specified named-topic variable, the mapping is ignored. However, if a variable is specified
	 * and the topic spec does not specify such a named-topic variable, the initialization fails.
	 * 
	 * @author renkenh
	 */
	@Documented
	@Retention(RUNTIME)
	@Target({TYPE, METHOD})
	public @interface TopicMapping
	{
		
		public static final int QOS_NOT_SET = -1;


		/**
		 * Returns the a part of the topic. May contain wild cards.
		 * @return A part of the topic.
		 */
		public String value() default "";

		/**
		 * The quality of service that is used to connect to this topic.
		 * @return The quality of service for the specified topic.
		 */
		public int qos() default QOS_NOT_SET;
		
		public String contentType() default "";

	}

	/**
	 * @author renkenh
	 *
	 */
	@Documented
	@Retention(RUNTIME)
	@Target(METHOD)
	public @interface PublishOptions
	{
		
		public enum Retain
		{
			TRUE,
			FALSE,
			UNDEFINED
		}
		
		public Retain retain() default Retain.UNDEFINED;
		
	}

	@Documented
	@Retention(RUNTIME)
	@Target(TYPE)
	/**
	 * @author renkenh
	 *
	 */
	public @interface ExceptionHandler {}

	
}

/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import cc.renken.mqtt.convert.ConvertException;
import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.api.IMQTTClient;
import cc.renken.mqtt.core.api.Topics;
import cc.renken.mqtt.spring.MQTT;
import cc.renken.mqtt.spring.MQTT.TopicMapping;
import cc.renken.mqtt.spring.config.SpringClientConfiguration;
import cc.renken.mqtt.spring.config.SpringClientOptions;
import cc.renken.mqtt.spring.util.LastWillUtil;

/**
 * A proxy per annotated class.
 * @author renkenh
 */
class PublishProxy implements InvocationHandler
{
	
	private static final Logger logger = LoggerFactory.getLogger(PublishProxy.class);

	
	private final IMQTTClient client;
	private final Map<Method, PublishMethodAdapter> pushers = new HashMap<>();
	

	public PublishProxy(Class<?> clazz, PublishProxyAdapterFactory factory, LastWillUtil lwUtil, ISpringMQTTService service) throws MqttException, ConvertException
	{
		SpringClientConfiguration clientConfig = new SpringClientConfiguration(createOptionsFrom(clazz, lwUtil, service.getConfiguration().getDefaultContentType()), service.getConfiguration());
		this.client = service.getClient(clientConfig);
		Arrays.asList(clazz.getMethods()).stream().forEach((m) -> {
			try
			{
				PublishMethodAdapter adapter = factory.generateAdapter(m, clientConfig);
				if (adapter != null)
					this.pushers.put(m, adapter);
			}
			catch (ConvertException | MqttException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	
	@Override
	public Object invoke(Object target, Method method, Object[] callArgs) throws Exception
	{
		if ("hashCode".equals(method.getName()) && method.getParameterTypes().length == 0 && method.getReturnType().equals(int.class))
			return this.hashCode();
		if ("equals".equals(method.getName()) && method.getParameterTypes().length == 1 && method.getReturnType().equals(boolean.class))
			return this.equals(callArgs[0]);
		
		PublishMethodAdapter adapter = this.pushers.get(method);
		if (adapter == null)
			throw new UnsupportedOperationException("Could not find proper definition for method " + target.getClass().getName() + "#" + method.getName());
		
		try
		{
			this.client.connect();
			this.client.publish(adapter.computeTopic(callArgs), adapter.computeBody(callArgs), adapter.computeQoS(callArgs), adapter.computeRetain(callArgs), adapter.computeMSGId(callArgs));
		}
		catch (MqttException ex)
		{
			if (adapter.throwsMqttException())
				throw ex;
			logger.warn("Got an exception when trying to execute {} with {}", method.toGenericString(), Arrays.stream(callArgs).map((o) -> String.valueOf(o)).collect(Collectors.joining(",")), ex);
		}
		return null;
	}

	private static final SpringClientOptions createOptionsFrom(Class<?> clazz, LastWillUtil lwUtil, ContentType defaultContentType) throws IllegalArgumentException, ConvertException
	{
		SpringClientOptions options = new SpringClientOptions();
		MQTT.Publisher publisher = clazz.getAnnotation(MQTT.Publisher.class);
		MQTT.TopicMapping mapping = clazz.getAnnotation(MQTT.TopicMapping.class);
		if (StringUtils.hasText(publisher.lastWill()) && (StringUtils.hasText(publisher.lastWillDestination()) || mapping != null && StringUtils.hasText(mapping.value())))
		{
			options.setLastWill(lwUtil.computeLastWill(publisher, mapping, defaultContentType));
			if (mapping != null && StringUtils.hasText(mapping.value()))
				options.setLastWillDestination(mapping.value());
			if (StringUtils.hasText(publisher.lastWillDestination()))
				options.setLastWillDestination(publisher.lastWillDestination());
		}
		if (mapping != null && StringUtils.hasText(mapping.value()))
			options.setTopicBase(Topics.get(mapping.value()));
		if (mapping != null && StringUtils.hasText(mapping.contentType()))
			options.setContentType(ContentType.valueOf(mapping.contentType()));
		if (mapping != null && mapping.qos() != TopicMapping.QOS_NOT_SET)
			options.setQoS(mapping.qos());
		return options;
	}
}

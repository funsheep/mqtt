/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import cc.renken.mqtt.core.api.IMQTTService;
import cc.renken.mqtt.spring.config.SpringServiceConfiguration;

/**
 * @author renkenh
 *
 */
public interface ISpringMQTTService extends IMQTTService
{

	@Override
	public SpringServiceConfiguration getConfiguration();
}

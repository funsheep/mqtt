/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.Set;
import java.util.stream.Collectors;

import cc.renken.mqtt.core.api.Topic;
import cc.renken.mqtt.spring.MQTT.TopicMapping;
import cc.renken.mqtt.spring.MQTT.Param.Type;

/**
 * @author renkenh
 *
 */
class Validation
{

	public static final boolean validateReceiveMethod(Method method)
	{
		if (method.getAnnotation(TopicMapping.class) == null && !ReceiveMethodAdapterFactory.TOPIC_PATTERN.matcher(method.getName()).matches())
			return false;
		Topic topic = new MethodConfiguration(method, null).getTopic();
		ParameterDefinition def = new ParameterDefinition(method);
		
		final Set<String> toCheck = def.getParameters().stream().filter((p) -> p.type == Type.TOPIC_PARAM).map((p) -> p.name).collect(Collectors.toSet());
		final Set<String> against = topic.getNamedParameters().stream().map((p) -> p.getName()).collect(Collectors.toSet());
		for (String check : toCheck)
			if (!against.remove(check))
				throw new IllegalArgumentException("Found orphran parameter "+check+" in method call "+method.toString());
		return true;
	}

	public static final boolean validatePublishMethod(Method method)
	{
		if (method.getAnnotation(TopicMapping.class) == null && !PublishProxyAdapterFactory.TOPIC_PATTERN.matcher(method.getName()).matches())
			return false;
		Topic topic = new MethodConfiguration(method, null).getTopic();
		ParameterDefinition def = new ParameterDefinition(method);
		
		final Set<String> toCheck = topic.getNamedParameters().stream().map((p) -> p.getName()).collect(Collectors.toSet());
		final Set<String> against = def.getParameters().stream().filter((p) -> p.type == Type.TOPIC_PARAM).map((p) -> p.name).collect(Collectors.toSet());
		for (String check : toCheck)
			if (!against.remove(check))
				throw new IllegalArgumentException("Found orphran parameter "+check+" in topic of method call "+method.toString());
		return true;
	}


	private Validation()
	{
		//no instance
	}

}

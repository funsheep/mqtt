/**
 * 
 */
package cc.renken.mqtt.spring.config;

import org.springframework.util.StringUtils;

import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.config.ClientConfiguration;

/**
 * @author renkenh
 *
 */
public class SpringClientConfiguration extends ClientConfiguration
{
	
	private ContentType contentType;


	/**
	 * @param config
	 */
	public SpringClientConfiguration(SpringClientConfiguration config)
	{
		super(config);
		this.contentType = config.contentType;
	}

	/**
	 * @param clientOptions
	 * @param serviceConfig
	 */
	public SpringClientConfiguration(SpringClientOptions clientOptions, SpringServiceConfiguration serviceConfig)
	{
		super(clientOptions, serviceConfig);
		this.contentType = clientOptions.getContentType();
	}

	/**
	 * @return the lastWillDestination
	 */
	@Override
	public String getLastWillDestination()
	{
		String destination = super.getLastWillDestination();
		if (StringUtils.hasText(destination))
			return destination;
		return this.defaultConfiguration().getLastWillDestination();
	}

	/**
	 * @return the lastWill
	 */
	@Override
	public byte[] getLastWill()
	{
		byte[] lw = super.getLastWill();
		if (lw != null)
			return lw;
		return this.defaultConfiguration().getLastWill();
	}

	public ContentType getContentType()
	{
		if (this.contentType != null)
			return this.contentType;
		return this.defaultConfiguration().getDefaultContentType();
	}

	@Override
	protected SpringServiceConfiguration defaultConfiguration()
	{
		return (SpringServiceConfiguration) super.defaultConfiguration();
	}

}

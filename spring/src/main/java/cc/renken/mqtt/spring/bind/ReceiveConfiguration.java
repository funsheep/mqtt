/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import cc.renken.mqtt.spring.MQTT;

/**
 * @author renkenh
 *
 */
@Component
@ComponentScan(basePackages="cc.renken.mqtt.spring")
public class ReceiveConfiguration implements ApplicationListener<ContextRefreshedEvent>
{
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private ReceiveMethodAdapterFactory factory;
	@Autowired
	private ISpringMQTTService service;

	private final Map<String, ReceiveProxy> adaptersByBeanName = new HashMap<>();


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event)
	{
		Map<String, Object> receivers = this.context.getBeansWithAnnotation(MQTT.Receiver.class);
		for (Entry<String, Object> entry : receivers.entrySet())
		{
			try
			{
				this.adaptersByBeanName.put(entry.getKey(), new ReceiveProxy(entry.getValue(), this.factory, this.service));
			}
			catch (MqttException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//FIXME lifecycle
}

/**
 * 
 */
package cc.renken.mqtt.spring.convert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.renken.mqtt.convert.payload.APayloadConverterService;
import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.convert.payload.IPayloadConverter;

/**
 * @author renkenh
 *
 */
@Component
public class MQTTConverterService extends APayloadConverterService
{
	
	@SuppressWarnings("unchecked")
	private final List<IPayloadConverter<?>>[] convertersByContentType = new List[ContentType.values().length];


	@Autowired
	private void postConstruct(Collection<IPayloadConverter<?>> converters)
	{
		for (int i = 0; i < this.convertersByContentType.length; i++)
			this.convertersByContentType[i] = new ArrayList<>(2);
		for (IPayloadConverter<?> converter : converters)
		{
			for (ContentType ct : converter.supportedContentTypes())
				this.convertersByContentType[ct.ordinal()].add(converter);
		}
	}

	@Override
	protected List<IPayloadConverter<?>> getConverters(ContentType contentType)
	{
		return this.convertersByContentType[contentType.ordinal()];
	}

}

/**
 * 
 */
package cc.renken.mqtt.spring.bind;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import cc.renken.mqtt.spring.MQTT;
import cc.renken.mqtt.spring.MQTT.Param.Type;
import cc.renken.mqtt.spring.bind.ParameterDefinition.Parameter;

/**
 * @author renkenh
 *
 */
final class ParameterFactory
{

	static final List<Parameter> extractParameters(Method method)
	{
		List<Parameter> params = new ArrayList<>();
		int index = 0;
		for (java.lang.reflect.Parameter parameter : method.getParameters())
		{
			String name = extractName(parameter);
			Type type = extractType(parameter);
			boolean isRequired = extractIsRequired(parameter);
			params.add(new Parameter(name, index, type, parameter.getType(), isRequired));
			index++;
		}
		return params;
	}

	private static final boolean extractIsRequired(java.lang.reflect.Parameter parameter)
	{
		MQTT.Param paramMapping = parameter.getAnnotation(MQTT.Param.class);
		if (paramMapping != null)
			return paramMapping.isRequired();
		return MQTT.Param.DEFAULT_ISREQUIRED;
	}
	
	private static final String extractName(java.lang.reflect.Parameter parameter)
	{
		MQTT.Param paramMapping = parameter.getAnnotation(MQTT.Param.class);
		if (paramMapping != null && StringUtils.hasText(paramMapping.name()))
			return paramMapping.name();
		return parameter.getName();
	}
	
	private static final MQTT.Param.Type extractType(java.lang.reflect.Parameter parameter)
	{
		MQTT.Param paramMapping = parameter.getAnnotation(MQTT.Param.class);
		if (paramMapping != null)
			return paramMapping.value();
		
		switch (parameter.getName().toLowerCase())
		{
			case "body":
			case "payload":
				return Type.PAYLOAD;
			case "qos":
				return Type.QoS;
			case "retain":
			case "retained":
				return Type.RETAIN;
			case "topic":
				return Type.TOPIC;
			case "msgid":
			case "messageid":
				return Type.MSG_ID;
			default:
				return Type.TOPIC_PARAM;
		}
	}
	
	/**
	 * 
	 */
	private ParameterFactory()
	{
		//no instance
	}

}

/**
 * 
 */
package cc.renken.mqtt.spring.config;

import cc.renken.mqtt.convert.payload.ContentType;
import cc.renken.mqtt.core.config.ServiceOptions;

/**
 * @author renkenh
 *
 */
public class SpringServiceOptions extends ServiceOptions
{
	
	private ContentType contentType;


	/**
	 * 
	 */
	public SpringServiceOptions()
	{
		//do nothing
	}

	/**
	 * @param options
	 */
	public SpringServiceOptions(SpringServiceOptions options)
	{
		super(options);
		this.contentType = options.contentType;
	}

	public void setDefaultContentType(ContentType contentType)
	{
		this.contentType = contentType;
	}

	public void setLastWillDestination(String lastWillDestination)
	{
		super.setLastWillDestination(lastWillDestination);
	}

	public void setLastWill(byte[] lastWill)
	{
		super.setLastWill(lastWill);
	}

	ContentType getContentType()
	{
		return contentType;
	}

}
